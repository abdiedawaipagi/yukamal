/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	config.toolbarGroups = [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles'] },
		{ name: 'paragraph', groups: [ 'list', 'align'] },
		{ name: 'styles' },
	];
	 
	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Subscript,Superscript';
	 
	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
	 
	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};
