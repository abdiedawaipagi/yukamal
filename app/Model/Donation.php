<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\DonationYukamal;

class Donation extends Model
{
    protected $table = 'donation';
    public $timestamps = false;

    static function getDonation($id){
    	$data = DB::table('donation as d')
    	->leftJoin('mosque as m','m.id','d.mosque_identity')
    	->leftJoin('users as u','u.id','d.user_id')
    	->select('m.name as mosque_name','d.*','u.name as user_name')
        ->where('u.id',$id)
    	->get();

    	foreach ($data as $d) {
            $total = $d->nominal;
            $finance = DonationYukamal::where('no_trans',$d->no_trans)->get();
            foreach ($finance as $f) {
                $total += floatval($f->nominal);
            }
            $d->total = $total;
        }
        return $data;
    }

    static function total_donation($id){
        $data = DB::table('donation as d')->select('d.*')->where('d.id',$id)->first();

        $total = $data->nominal;
        $finance = DonationYukamal::where('no_trans',$data->no_trans)->get();
        foreach ($finance as $f) {
            $total += floatval($f->nominal);
        }
        $data->total = $total;

        return $data;
    }

    static function getDonations(){
        $data = DB::table('donation as d')
        ->leftJoin('mosque as m','m.id','d.mosque_identity')
        ->leftJoin('users as u','u.id','d.user_id')
        ->select('m.name as mosque_name','d.*','u.name as user_name')
        ->get();
        return $data;
    }
}
