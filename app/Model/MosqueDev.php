<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueDev extends Model
{
    protected $table = 'mosque_dev';
    public $timestamps = false;

    static function getPembangunanMasjid(){
    	$data = DB::table('mosque_dev as md')->join('mosque as m','m.id','md.mosque_id')->join('dev_weight as dw','dw.id','md.dev_id')->select('md.*','dw.name as dev_name','m.name as mosque_name')->get();
    	return $data;
    } 

    static function getPembangunanMasjidDkm($id){
    	$data = DB::table('mosque_dev as md')
    	->join('mosque as m','m.id','md.mosque_id')
    	->join('dev_weight as dw','dw.id','md.dev_id')
    	->select('md.*','dw.name as dev_name','m.name as mosque_name')
    	->where('m.id',$id)
    	->get();
    	return $data;
    } 
}
