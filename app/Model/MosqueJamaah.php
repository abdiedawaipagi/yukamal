<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueJamaah extends Model
{
    protected $table = 'mosque_ummat';
    public $timestamps = false;

    static function getJamaah(){
    	$data = DB::table('mosque_ummat as mu','m.name')
            ->join('mosque as m','m.id','mu.mosque_id')
            ->join('user_profile as up','mu.user_id','up.user_id')
            ->get();
    	return $data;
    }

    static function getJamaahDkm($id){
        $data = DB::table('mosque_ummat as mu')
        ->join('mosque as m','m.id','mu.mosque_id')
        ->join('user_profile as up','mu.user_id','up.user_id')
        ->where('mu.mosque_id',$id)
        ->get();
        return $data;
    }

    static function jamaah($id){
    	$data = DB::table('mosque_ummat as mu')
        ->join('user_profile as up','mu.user_id','up.user_id')
        ->select('up.*',DB::raw('YEAR(CURDATE()) - YEAR(dob) as age'))
        ->where('mu.mosque_id',$id)->get();
    	return $data;
    } 

    static function total_jamaah(){
        $data = DB::select('select count(total) total from (select count(id) total from mosque_ummat GROUP BY user_id) user');
        return $data;
    }
}
