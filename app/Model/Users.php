<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Users extends Model
{
    protected $table = 'users';
    public $timestamps = false;

}
