<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DevWeight extends Model
{
    protected $table = 'dev_weight';
    public $timestamps = false;
}
