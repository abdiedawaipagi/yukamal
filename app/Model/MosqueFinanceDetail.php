<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MosqueFinanceDetail extends Model
{
	protected $table = 'mosque_finance_detail';
    public $timestamps = false;

    static function getDetail($id){
    	$data = DB::table('mosque_finance_detail as m')
    	->join('mosque_finance as mf','m.category_id','mf.id')
    	->join('category as c','m.category_id','c.id')
    	->select('m.*','c.nama','mf.date')
    	->where('mf.mosque_id',$id)
    	->get();
    	return $data;
    }
}
