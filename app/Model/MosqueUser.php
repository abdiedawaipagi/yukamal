<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MosqueUser extends Model
{
    protected $table = 'mosque_user';
    public $timestamps = false;
}
