<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Districts extends Model
{
    protected $table = 'districts';
    public $timestamps = false;

    public static function getDisctricts()
    {
        $disctricts = DB::table('districts as d')->leftJoin('regencies as r','d.regency_id','r.id')->select('d.*','r.name as regency_name')->orderBy('d.name', 'asc')->get();
        return $disctricts;
    }
}
