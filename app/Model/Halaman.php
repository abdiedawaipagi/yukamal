<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Halaman extends Model
{
    protected $table = 'page';
    public $timestamps = false;
}
