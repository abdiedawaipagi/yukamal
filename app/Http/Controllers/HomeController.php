<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Model\Mosque;
use App\Model\MosqueDkm;
use App\Model\MosqueEvent;
use App\Model\MosqueFinance;
use App\Model\MosqueFinanceDetail;
use App\Model\MosqueJamaah;
use App\Model\MosqueGaleri;
use App\Model\NewsModel;
use App\Model\Donation;
use App\Model\Users;
use App\Model\TeamModel;
use App\Model\DonationYukamal;
use App\Model\Provinces;
use App\Model\UserProfile;
use App\Model\VisMis;
use App\Model\MosqueDev;
use App\Model\DevWeight;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    // controller view home / beranda
    public function index()
    {
        if (!Auth::guest()) {
            $role = Auth::user()->role;
            // if ($role == 'admin' || $role == 'dkm') {
            //     return redirect('admin');
            // }
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.home';
        $data['title'] = 'YUKAMAL';
        $data['masjid'] = Mosque::getAll(); //mengambil semua data masjid
        $data['acara'] = MosqueEvent::getEvent(); //mengambil semua data acara
        $data['donatur'] = Donation::select(DB::raw('count(id) as total'))->where('status',2)->first(); //jumlah donatur
        $data['jamaah'] = MosqueJamaah::total_jamaah()[0]; //jumlah jamaah
        $data['mesjid'] = Mosque::select(DB::raw('count(id) as total'))->where('type','0')->first(); //jumlah masjid
        $data['musholla'] = Mosque::select(DB::raw('count(id) as total'))->where('type','1')->first(); // jumlah musholla
        $data['donasi_all'] = Donation::select(DB::raw('sum(nominal) as total'))->where('status','2')->first(); // jumlah seluruh donasi
        return view('layout.frontend.app', $data);
    }

    //controller view regiter
    public function register(){
        $data['view'] = 'frontend.register';
        $data['title'] = 'YUKAMAL';
        return view('layout.frontend.nofooter', $data);
    }

    //controller view donasi
    public function donasi(Request $request){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.donasi';
        $data['title'] = 'YUKAMAL';
        if($request->kecamatan == NULL){
            //jika tidak ada inputan search, mengambil semua data masjid
            $data['masjid'] = Mosque::getAll();
        }else{
            //jika ada inputan search, mengambil semua data masjid berdasarkan input search
            $dats['kec_id'] = $request->kecamatan;
            $dats['search'] = $request->search;
            $data['masjid'] = Mosque::getSearch($dats);
        }
        $data['provinces'] = Provinces::All();
        return view('layout.frontend.app', $data);
    }

    //controller view acara
    public function acara(Request $request){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.acara';
        $data['title'] = 'YUKAMAL';
        if($request->search == NULL){
            //jika tidak ada inputan search, mengambil semua data acara
            $data['acara'] = MosqueEvent::acara();
        }else{
            //jika ada inputan search, mengambil semua data acara berdasarkan input search
            $dats['search'] = $request->search;
            $data['acara'] = MosqueEvent::acaraSearch($dats);
        }
        $data['provinces'] = Provinces::All();
        return view('layout.frontend.app', $data);
    }

    //controller view berita
    public function berita(){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.berita';
        $data['title'] = 'YUKAMAL';
        $data['provinces'] = Provinces::All();
        $data['berita'] = DB::table('news')->orderBy('id','desc')->paginate(9); //get berita , dengan tag pagination 9 berita/perpage
        return view('layout.frontend.app', $data);
    }

    //controller view detail berita
    public function detailberita($id){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.detailberita';
        $data['title'] = 'YUKAMAL';
        $data['berita'] = NewsModel::where('id',$id)->first();
        return view('layout.frontend.app', $data);
    }

    //controller view detail acara
    public function detailacara($id){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.detailacara';
        $data['title'] = 'YUKAMAL';
        $data['acara'] = MosqueEvent::where('id',$id)->first();
        return view('layout.frontend.app', $data);
    }

    //controller view tentang kami
    public function about(){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }else{
            $data['user'] = '';
        }
        $data['view'] = 'frontend.about';
        $data['title'] = 'YUKAMAL';
        $data['team'] = TeamModel::all();
        return view('layout.frontend.app', $data);
    }

    //controller view detail donasi
    public function detail($id){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }

        $mosqueDetail = Mosque::getDetail($id);

        if (is_null($mosqueDetail)) {
            return redirect('/');
        }
        
        $data['view'] = 'frontend.detail';
        $data['title'] = 'YUKAMAL';
        $data['masjid'] = Mosque::getDetail($id);
        $data['mosque_gallery'] = Mosque::getGallery($id);
        $data['mosque_event'] = MosqueEvent::where('mosque_id',$id)->get();
        $data['pengurus'] = MosqueDkm::where('mosque_identity',$id)->get();
        $data['total_donasi'] = Donation::select(DB::raw('sum(nominal) as total'))->where('mosque_identity',$id)->where('status',2)->first();
        $data['jamaah'] = MosqueJamaah::jamaah($id);
        $finance = MosqueFinance::getKeuanganByMosqueId($id);
        $return = [];

        for ($i=1; $i <= 12; $i++) {
            $income = 0;
            $outcome = 0;
            $month = $i; 
            foreach ($finance as $f) {
                if ($f->month == $i) {
                    if ($f->category == 'Pendapatan') {
                        $income += floatval($f->nominal);
                    } else {
                        $outcome += floatval($f->nominal);
                    }
                }
            }
            $return[$i] = [
                'month' => $month,
                'pendapatan' => $income,
                'pengeluaran' => $outcome,
            ];
        }
        $data['finance'] = $return;
        $data['table_finance'] = MosqueFinance::getKeuanganByMosqueId($id);
        $data['visi_misi'] = VisMis::getVisMisByMosqueId($id);
        $data['dev'] = MosqueDev::getPembangunanMasjidDkm($id);
        $data['dev_master'] = DevWeight::All();
        return view('layout.frontend.app', $data);
    }

    //controller view profile user
    public function profile(){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.profile';
        $data['title'] = 'YUKAMAL';
        $data['users'] = UserProfile::getProfile2(Auth::user()->id);
        $data['masjid'] = Mosque::all();
        $data['jamaah'] = MosqueJamaah::where('user_id',Auth::user()->id)->get();
        return view('layout.frontend.nofooter', $data);
    }

    //controller view lupa password
    public function forgot_password(){
    $data['view'] = 'frontend.forgot';
    $data['title'] = 'YUKAMAL';
    return view('layout.frontend.nofooter', $data);   
    }

    //controller view proses donasi
    public function proses($id){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.add';
        $data['title'] = 'YUKAMAL';
        $data['users'] = Auth::user();
        $data['masjid'] = Mosque::where('id',$id)->first();
        return view('layout.frontend.nofooter', $data);
    }

    //controller view detail transaksi donasi
    public function payment($id){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.payment';
        $data['title'] = 'YUKAMAL';
        $data['donation'] = Donation::total_donation($id);
        return view('layout.frontend.app', $data);
    }

    //controller view list transaksi saya
    public function mytransaction(){
        if (!Auth::guest()) {
            $data['user'] = UserProfile::where('user_id',Auth::user()->id)->first(); // data user login
            $data['total_trans'] = Donation::select(DB::raw('count(id) as total'))->where('user_id',Auth::user()->id)->where('status','0')->orWhere('status','3')->first(); //total transaksi user
        }
        $data['view'] = 'frontend.mytransaction';
        $data['title'] = 'YUKAMAL';
        $id = Auth::user()->id;
        $data['transaction'] = Donation::getdonation($id);
        return view('layout.frontend.app', $data);   
    }

    //controller submit donasi
    public function store(Request $request)
    {
        try {
            $donation = new Donation;
            $donation->mosque_identity = $request->mosque_identity;
            $donation->user_id = $request->user_id;
            $donation->bank = $request->bank;
            $random = Str::random(8);
            $donation->no_trans = $random;
            if(!$request->anon){
                $donation->contributor_name = $request->contributor_name;
            }else{
                $donation->contributor_name = $request->anon;
            }
            $donation->sub_category_id = $request->sub_category_id;
            $donation->nominal = str_replace(',', '', $request->nominal);
            $donation->save();

            if($request->donasi_yukamal != null){
                $yukamal = new DonationYukamal;
                $yukamal->no_trans = $random; 
                $yukamal->user_id = $request->user_id;
                if(!$request->anon){
                    $yukamal->contributor_name = $request->contributor_name;
                }else{
                    $yukamal->contributor_name = $request->anon;
                }
                $yukamal->nominal = str_replace(',', '', $request->donasi_yukamal);
                $yukamal->save();
            }
            return redirect('payment/'.$donation->id);
        } catch (Exception $e) {
            return $e->getMessage();   
        }
    }

    //controller submit donasi yukamal
    public function donasi_yukamal(Request $request){
        try{
            $yukamal = new DonationYukamal;
            $random = Str::random(8);
            $yukamal->no_trans = $random; 
            $yukamal->user_id = $request->user_id;
            if(!$request->contributor_name){
                $yukamal->contributor_name = $request->contributor_name;
            }else{
                $yukamal->contributor_name = $request->anon;
            }
            $yukamal->nominal = str_replace(',', '', $request->nominal);
            $yukamal->save();
            return redirect('about');
        } catch (Exception $e){
            return $e->getMessage();   
        }
    }

    //controller update profile
    public function update_profile(Request $request)
    {
        try {
            $users = Users::where('id',$request->user_id)->first();
            $users->username = $request->username;
            $users->email = $request->email;
            $users->update();

            $userp = UserProfile::where('user_id',$request->user_id)->first();
            $userp->first_name = $request->first_name;
            $userp->last_name = $request->last_name;
            $userp->dob = $request->dob;
            $userp->gender = $request->gender;
            $userp->update();

            $recent = MosqueJamaah::where('user_id',$request->user_id)->get();
            foreach ($recent as $value) {
                $del = MosqueJamaah::where('user_id',$value->user_id)->first();
                $del->delete();
            }

            if($request->jamaah != null){
                foreach ($request->jamaah as $value) {
                    $c_jamaah = new MosqueJamaah;
                    $c_jamaah->user_id = $request->user_id;
                    $c_jamaah->mosque_id = $value['mosque_id'];
                    $c_jamaah->save();
                }
            }

                
            return redirect('profile');
        } catch (Exception $e) {
            return $e->getMessage();   
        }
    }

    //controller update password
    public function update_password(Request $request){
        try {
            $users = Users::where('id',$request->id)->first();
            $users->password = bcrypt($request->password); //encrypt password
            $users->update();
            return redirect('login');
        } catch (Exception $e) {
            return $e->getMessage();   
        }   
    }

    //controller fungsi update password di lupa password
    public function proses_password(Request $request){
        try {
            $users = Users::where('email',$request->user_email)->first();
            if(is_null($users)){
                return redirect('forgot_password');
            }else{
                $users->password = bcrypt($request->new_password);
                $users->update();
                return redirect('login');
            }
        } catch (Exception $e) {
            return $e->getMessage();   
        }   
    }

    //controller update bukti transfer
    public function fileupload(Request $request){
        if ($request->hasFile('image')) {  //check the file present or not
            $id = $request->donation_id;
            $donatur = str_replace(' ', '', $request->donatur);;
            $donation = Donation::where('id',$id)->first(); // get data donasi berdasarkan id donasi
            $image = $request->file('image'); //get the file
            $name = $donatur.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads'); //public path folder dir
            $image->move($destinationPath, $name);
            $donation->pic = $name;
            $donation->status = '3';
            $donation->update();
        }
        return redirect('mytransaction');
    }

    //controller batalkan transaksi
    public function cancel($id){
        $donation = Donation::where('id',$id)->first();
        $donation->status = '1';
        $donation->update();
        return redirect('mytransaction');
    }

    //controller export excel
    public function export_excel_finance($mosqueId){
        $data = MosqueFinance::getKeuanganByMosqueId($mosqueId);
        $data = json_decode(json_encode($data), true);
        $masjidName = Mosque::find($mosqueId)->name;
        $xl = [];

        foreach ($data as $d) {
            $xl[] = [
                'tanggal' => $d['date'],
                'cat' => $d['category'],
                'sub-cat' => $d['sub_category'],
                'info' => $d['information'],
                'nominal' => $d['nominal'],
            ];
        }

        $header = array('TANGGAL','KATEGORI','SUB-KATEGORI','INFORMASI','NOMINAL');
        
        Excel::create('data_keuangan_masjid/mushola_'.$masjidName.'_tahun_'.date('Y'), function($excel) use ($xl, $header, $masjidName){
            $excel->sheet('sheet1', function($sheet) use ($xl, $header, $masjidName){
                $sheet->mergeCells('A1:E1');
                $sheet->cells('A1', function ($cells) use ($masjidName) {
                    $cells->setFontSize(14);
                    $cells->setFontWeight();
                    $cells->setValue('Data Keuangan Masjid '.$masjidName.' Periode Tahun '.date('Y'));
                });
                $sheet->cells('A3:E3', function ($cells) {
                    $cells->setFontSize(12);
                    $cells->setBackground('#6495ed');
                });
                $sheet->setColumnFormat(array(
                    'A:E' => '@'
                ));
                $sheet->row(3,$header);
                $sheet->fromArray($xl,null,'A4',null,false);
            });
        })->download('xls');
    }

    //controller export pdf
    public function export_pdf_finance($mosqueId){
        $data = MosqueFinance::getKeuanganByMosqueId($mosqueId);
        $masjidName = Mosque::find($mosqueId)->name;

        $pdf = PDF::loadView('frontend.pdf_finance', ['data' => $data, 'masjid' => $masjidName]);
        return $pdf->download('data_keuangan_masjid_mushola_'.$masjidName.'_tahun_'.date('Y').'.pdf');
    }
}