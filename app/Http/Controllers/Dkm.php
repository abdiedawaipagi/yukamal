<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\MosqueDkm;
use App\Model\MosqueDkmUser;
use App\Model\MosqueUser;
use App\Model\Mosque;
use App\Image_uploaded;
use Image;
use File;

class Dkm extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        $data['view'] = 'backend.admin.dkm';
        $data['title'] = 'Pengurus';
        $data['page'] = 'dkm';
        $data['role'] = $role;
        if ($role == 'admin') {
            $data['data'] = MosqueDkm::getDkm();
        } else {
            $MosqueDkmUser = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $data['data'] = MosqueDkm::getDkmPengurus($MosqueDkmUser->mosque_id);
        }
        
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.dkm_create';
        $data['title'] = 'Add Pengurus';
        $data['page'] = 'dkm';
        $data['mosque'] = Mosque::All();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dkm = new MosqueDkm;
        $dkm->name = $request->name;
        $dkm->position = $request->position;
        $dkm->mosque_identity = $request->mosque_identity;

        if (isset($request->image)) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpg,png,jpeg'
            ]);
            
            //JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path)) {
                //MAKA FOLDER TERSEBUT AKAN DIBUAT
                File::makeDirectory($this->path);
            }
            
            //MENGAMBIL FILE IMAGE DARI FORM
            $file = $request->file('image');
            //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
            $fileName = 'dkm_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
            //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
            Image::make($file)->save($this->path . '/' . $fileName);

            $dkm->pic = $fileName;
        }
            
        $dkm->save();

        return redirect('admin/dkm')->with('success', 'Tambah Pengurus Berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.dkm_show';
        $data['title'] = 'Detail Pengurus';
        $data['page'] = 'dkm';
        $data['data'] = MosqueDkm::where('id',$id)->first();
        $data['mosque'] = Mosque::All();
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.dkm_edit';
        $data['title'] = 'Edit Pengurus';
        $data['page'] = 'dkm';
        $data['data'] = MosqueDkm::where('id',$id)->first();
        $data['mosque'] = Mosque::All();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dkm = MosqueDkm::where('id', $id)->first();
        $dkm->name = $request->name;
        $dkm->position = $request->position;
        $dkm->mosque_identity = $request->mosque_identity;

        if (isset($request->image)) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpg,png,jpeg'
            ]);
            
            //JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path)) {
                //MAKA FOLDER TERSEBUT AKAN DIBUAT
                File::makeDirectory($this->path);
            }
            //MENGAMBIL FILE IMAGE DARI FORM
            $file = $request->file('image');
            //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
            $fileName = 'dkm_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
            //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
            Image::make($file)->save($this->path . '/' . $fileName);

            File::delete($this->path.'/'.$request->image_old);

            $dkm->pic = $fileName;
        }else{
            if (empty($request->image_old)) {
                $dkm->pic = $request->image;

                File::delete($this->path.'/'.$request->image_name);
            }
        }

        $dkm->update();

        return redirect('admin/dkm')->with('success', 'Update Pengurus Berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dkm = MosqueDkm::where('id', $id)->first();
        File::delete($this->path.'/'.$dkm->pic);
        $dkm->delete();
        return redirect(route('dkm.index'))->with('success', 'Delete Pengurus Berhasil!');
    }
}
