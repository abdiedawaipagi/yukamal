<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Provinces;

class Provinsi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.provinsi';
            $data['title'] = 'Provinsi';
            $data['page'] = 'provinsi';
            $data['data'] = Provinces::All();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.provinsi_create';
            $data['title'] = 'Add Provinsi';
            $data['page'] = 'provinsi';
            $data['user_id'] = Auth::User()->id;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $exist = Provinces::where('name',$request->name)->exists();
            if ($exist) {
                return redirect(route('provinsi.index'))->with('alert', 'Nama Provinsi Sudah Terpakai!');
            }
            $prov = new Provinces;
            $prov->name = $request->name;
            $prov->save();

            return redirect(route('provinsi.index'))->with('success', 'Tambah Provinsi Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.provinsi_show';
            $data['title'] = 'Detail provinsi';
            $data['page'] = 'provinsi';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Provinces::where('id',$id)->first();
            $data['category'] = Provinces::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.provinsi_edit';
            $data['title'] = 'Edit provinsi';
            $data['page'] = 'provinsi';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Provinces::where('id',$id)->first();
            $data['category'] = Provinces::all();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $exist = Provinces::where('name',$request->name)->exists();
            if ($exist) {
                $exist = Provinces::where('name',$request->name)->where('id',$id)->exists();
                if (!$exist) {
                    return redirect(route('provinsi.index'))->with('alert', 'Nama Provinsi sudah terpakai!');
                }
            }
            $prov = Provinces::where('id',$id)->first();
            $prov->name = $request->name;
            $prov->update();
            return redirect(route('provinsi.index'))->with('success', 'Update Provinsi Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prov = Provinces::where('id', $id);
        $prov->delete();
        return redirect(route('provinsi.index'))->with('success', 'Delete provinsi Berhasil!');
    }
}
