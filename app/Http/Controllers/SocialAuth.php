<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Users;
use App\Model\UserProfile;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuth extends Controller
{
	public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = User::where('email', $user->email)->first();
        if ($authUser) {
            Auth::login($authUser, true);
        }else{
            $create = User::create([
                'name' => $user['name'],
                'username' => $user['name'],
                'email' => $user['email'],
                'password' => bcrypt($user['name']),
                'role' => 'member',
            ]);
            if($create->id != ''){
                $up = new UserProfile;
                $split = explode(" ", $user->name);
                $firstname = array_shift($split);
                $lastname  = implode(" ", $split);
                $up->user_id = $create->id;
                $up->first_name = $firstname;
                $up->last_name = $lastname;
                $up->provider = $provider;
                $up->provider_id = $user['id'];
                $up->pic = $user->avatar;
                $up->save();
                Auth::login($create, true);
            }else{
                 return abort(404);
            }
        }
        return redirect('/');
    }

    public function create_user(Request $request){
        try {
            $create = User::create([
                'name' => $request->first_name.' '.$request->last_name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'role' => 'member',
            ]);
            if($create->id != ''){
                $up = new UserProfile;
                $up->user_id = $create->id;
                $up->first_name = $request->first_name;
                $up->last_name = $request->last_name;
                $up->save();
                Auth::login($create, true);
            }else{
                 return abort(404);
            }
            return redirect('/');
        } catch (Exception $e) {
            return $e->getMessage();   
        }
    }

}