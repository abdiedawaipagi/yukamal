<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\VisMis;
use App\Model\Mosque;
use App\Model\MosqueDkmUser;

class VisiMisi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        $data['view'] = 'backend.admin.visimisi';
        $data['title'] = 'Visi & Misi';
        $data['page'] = 'visi-misi';
        $data['role'] = $role;
        if ($role == 'admin') {
            $data['data'] = VisMis::getVisMis();
        }else{
            $MosqueDkmUser = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $VisMis = VisMis::where('mosque_identity',$MosqueDkmUser->mosque_id)->first();
            if (is_null($VisMis)) {
                return redirect()->route('visimisi.create');
            } else {
                return redirect()->route('visimisi.show',$VisMis->id);
            }
        }
        
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.visimisi_create';
        $data['title'] = 'Add Visi & Misi';
        $data['page'] = 'visi-misi';
        $data['category'] = VisMis::all();
        $data['masjid'] = Mosque::all();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $visimisi = new VisMis;
            $visimisi->mosque_identity = $request->mosque_identity;
            $visimisi->description = $request->description;
            $visimisi->save();

            return redirect(route('visimisi.index'))->with('success', 'Tambah Visi & Misi Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.visimisi_show';
        $data['title'] = 'Detail Visi & Misi';
        $data['page'] = 'visi-misi';
        $data['user_id'] = Auth::User()->id;
        $data['data'] = VisMis::where('id',$id)->first();
        $data['category'] = VisMis::all();
        $data['masjid'] = Mosque::all();
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.visimisi_edit';
        $data['title'] = 'Edit Visi & Misi';
        $data['page'] = 'visi-misi';
        $data['user_id'] = Auth::User()->id;
        $data['data'] = VisMis::where('id',$id)->first();
        $data['category'] = VisMis::all();
        $data['masjid'] = Mosque::all();
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $visimisi = VisMis::where('id',$id)->first();
            $visimisi->mosque_identity = $request->mosque_identity;
            $visimisi->description = $request->description;
            $visimisi->update();

            return redirect(route('visimisi.index'))->with('success', 'Update Visi & Misi Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visimisi = VisMis::where('id', $id);
        $visimisi->delete();
        return redirect(route('visimisi.index'))->with('success', 'Delete Visi & Misi Berhasil!');
    }
}
