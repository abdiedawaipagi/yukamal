<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Mosque;
use App\Model\MosqueJamaah;
use App\Model\MosqueDkmUser;
use App\Model\Districts;
use App\Model\Provinces;
use App\Model\Regencies;
use App\Model\Villages;
use App\Model\Bank;
use App\User;
use App\Image_uploaded;
use Image;
use File;

class Masjid extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
        if (Auth::guest()) {
            return redirect('admin');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        $data['view'] = 'backend.admin.masjid';
        $data['title'] = 'Masjid';
        $data['page'] = 'informasi';
        $data['role'] = $role;
        if ($role == 'admin') {
            $data['data'] = Mosque::All();
        }else{
            $MosqueDkmUser = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $mosque = Mosque::where('id',$MosqueDkmUser->mosque_id)->first();
            return redirect()->route('masjid.show',$mosque->id);
        }

        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.masjid_create';
            $data['title'] = 'Add Informasi Masjid';
            $data['page'] = 'informasi';
            $data['role'] = $role;
            $data['users'] = User::select('id','name')->where('role', 'dkm')->get();
            $data['province'] = Provinces::orderBy('name', 'asc')->get();
            $data['banks'] = Bank::orderBy('name', 'asc')->get();

            return view('layout.backend.app', $data);
        } else {
            return redirect()->route('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_null($request->province_id) || is_null($request->city_id) || is_null($request->kec_id) || is_null($request->kel_id)) {
            return redirect('admin/masjid/create')->with('alert', 'Provinsi / Kota / Kecamatan / Kelurahan tidak boleh kosong!');
        }
        
        if (Auth::user()->role == 'admin') {
            try {
                $lc = Mosque::select('code')->where('type',$request->type)->orderBy('id','desc')->first();
                if (is_null($lc)) {
                    $code = $request->type == 0 ? 'MSJ000001' : 'MSH000001';
                } else {
                    $lastCode = substr($lc->code, 3);
                    $code = '';
                    for ($i=0; $i < strlen($lastCode)-strlen(intval($lastCode)); $i++) { 
                        $code .= '0';
                    }
                    $code .= intval($lastCode+1);
                    $code = $request->type == 0 ? 'MSJ'.$code : 'MSH'.$code;
                }
                
                $mosque = new Mosque;
                $mosque->type = $request->type;
                $mosque->code = $code;
                $mosque->name = $request->name;
                $mosque->identity = $request->identity;
                $mosque->surface_area = $request->surface_area;
                $mosque->building_area = $request->building_area;
                $mosque->los = $request->los;
                $mosque->since = $request->since;
                $mosque->bank_id = $request->bank_id;
                $mosque->rek = $request->rek;
                $mosque->estimate = $request->estimate;
                $mosque->estimate_date = $request->estimate_date;
                $mosque->address = $request->address;
                $mosque->latitude = $request->latitude;
                $mosque->longitude = $request->longitude;
                $mosque->province_id = $request->province_id;
                $mosque->city_id = $request->city_id;
                $mosque->kec_id = $request->kec_id;
                $mosque->kel_id = $request->kel_id;
                $mosque->description = $request->description;
                
                if (isset($request->image)) {
                    $this->validate($request, [
                        'image' => 'required|image|mimes:jpg,png,jpeg'
                    ]);
                    
                    //JIKA FOLDERNYA BELUM ADA
                    if (!File::isDirectory($this->path)) {
                        //MAKA FOLDER TERSEBUT AKAN DIBUAT
                        File::makeDirectory($this->path);
                    }
                    
                    //MENGAMBIL FILE IMAGE DARI FORM
                    $file = $request->file('image');
                    //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                    $fileName = 'masjid_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                    //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                    Image::make($file)->save($this->path . '/' . $fileName);

                    $mosque->pic = $fileName;
                }

                $mosque->save();

                return redirect('admin/masjid')->with('success', 'Daftar Informasi Masjid/Musholla Berhasil!');
            } catch (Exception $e) {
                return $e->getMessage();   
            }
        }else{
            return redirect()->route('admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Auth::user()->role;
        $mosque = Mosque::where('id',$id)->first();
        $data['data'] = $mosque;
        $data['view'] = 'backend.admin.masjid_show';
        $data['title'] = 'Detail Informasi Masjid';
        $data['page'] = 'informasi';
        $data['role'] = $role;
        $data['users'] = User::select('id','name')->where('role', 'dkm')->get();
        $data['provinces'] = Provinces::find($mosque->province_id);
        $data['regencies'] = Regencies::find($mosque->city_id);
        $data['districts'] = Districts::find($mosque->kec_id);
        $data['villages'] = Villages::find($mosque->kel_id);
        $data['bank'] = Bank::find($mosque->bank_id);

        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Auth::user()->role;
        $mosque = Mosque::where('id',$id)->first();
        $data['data'] = $mosque;
        $data['view'] = 'backend.admin.masjid_edit';
        $data['title'] = 'Edit Informasi Masjid';
        $data['page'] = 'informasi';
        $data['role'] = $role;
        $data['users'] = User::select('id','name')->where('role', 'dkm')->get();
        $data['province'] = Provinces::orderBy('name', 'asc')->get();
        $data['regencies'] = Regencies::where('province_id',$mosque->province_id)->orderBy('name', 'asc')->get();
        $data['districts'] = Districts::where('regency_id',$mosque->city_id)->orderBy('name', 'asc')->get();
        $data['villages'] = Villages::where('district_id',$mosque->kec_id)->orderBy('name', 'asc')->get();
        $data['banks'] = Bank::orderBy('name', 'asc')->get();

        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $mosque = Mosque::where('id',$id)->first();
            $mosque->type = $request->type;
            $mosque->name = $request->name;
            $mosque->identity = $request->identity;
            $mosque->surface_area = $request->surface_area;
            $mosque->building_area = $request->building_area;
            $mosque->los = $request->los;
            $mosque->since = $request->since;
            $mosque->bank_id = $request->bank_id;
            $mosque->rek = $request->rek;
            $mosque->estimate = $request->estimate;
            $mosque->estimate_date = $request->estimate_date;
            $mosque->address = $request->address;
            $mosque->latitude = $request->latitude;
            $mosque->longitude = $request->longitude;
            $mosque->province_id = $request->province_id;
            $mosque->city_id = $request->city_id;
            $mosque->kec_id = $request->kec_id;
            $mosque->kel_id = $request->kel_id;
            $mosque->description = $request->description;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'masjid_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $mosque->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $mosque->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }

            $mosque->update();

            return redirect('admin/masjid')->with('success', 'Update Informasi Masjid/Musholla Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $masjid = Mosque::where('id', $id)->first();
            File::delete($this->path.'/'.$masjid->pic);
            $masjid->delete();
            return redirect(route('masjid'))->with('success', 'Delete Masjid Berhasil!');
        }else{
            return redirect()->route('admin');
        }
    }

    //ambil data jamaah untuk halaman Data Jamaah
    public function jamaah(){
        $role = Auth::user()->role;
        $data['view'] = 'backend.admin.masjid_jamaah';
        $data['title'] = 'Jama\'ah';
        $data['page'] = 'data-jamaah';
        $data['role'] = $role;
        if ($role == 'admin') {
            $data['data'] = MosqueJamaah::getJamaah();
        } else {
            $MosqueDkmUser = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $data['data'] = MosqueJamaah::getJamaahDkm($MosqueDkmUser->mosque_id);
        }

        return view('layout.backend.app', $data);
    }
}
