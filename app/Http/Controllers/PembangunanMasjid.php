<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\PembangunanModel;
use App\Model\MosqueDev;
use App\Model\Mosque;
use App\Model\MosqueDkmUser;

class PembangunanMasjid extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.pembangunan_masjid';
        $data['title'] = 'Pembangunan Masjid';
        $data['page'] = 'pembangunan-masjid';
        if ($data['role'] == 'admin') {
            $data['data'] = MosqueDev::getPembangunanMasjid();
        } else {
            $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $data['data'] = MosqueDev::getPembangunanMasjidDkm($data['MosqueDkmUser']->mosque_id);
        }
        
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.pembangunan_masjid_create';
        $data['title'] = 'Add Pembangunan Masjid';
        $data['page'] = 'pembangunan-masjid';
        $data['data'] = MosqueDev::All();
        $data['masjid'] = Mosque::All();
        $data['dev'] = PembangunanModel::All();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cat = new MosqueDev;
            $cat->mosque_id = $request->mosque_id;
            $cat->dev_id = $request->dev_id;
            $cat->weight = $request->weight;
            $cat->save();

            return redirect(route('pembangunan_masjid.index'))->with('success', 'Tambah Sub Kategori Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.pembangunan_masjid_show';
        $data['title'] = 'Detail Pembangunan Masjid';
        $data['page'] = 'pembangunan-masjid';
        $data['data'] = MosqueDev::find($id);
        $data['dev'] = PembangunanModel::find($data['data']['dev_id']);
        $data['masjid'] = Mosque::find($data['data']['mosque_id']);
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.pembangunan_masjid_edit';
        $data['title'] = 'Edit Pembangunan Masjid';
        $data['page'] = 'pembangunan-masjid';
        $data['data'] = MosqueDev::find($id);
        $data['dev'] = PembangunanModel::all();
        $data['masjid'] = Mosque::all();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cat = MosqueDev::find($id);
            $cat->mosque_id = $request->mosque_id;
            $cat->dev_id = $request->dev_id;
            $cat->weight = $request->weight;
            $cat->update();

            return redirect(route('pembangunan_masjid.index'))->with('success', 'Update Pembangunan Masjid Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = MosqueDev::find($id);
        $cat->delete();
        return redirect(route('pembangunan_masjid.index'))->with('success', 'Delete Pembangunan Masjid Berhasil!');
    }
}
