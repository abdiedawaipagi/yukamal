<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Villages;
use App\Model\Districts;

class Kelurahan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.kelurahan';
            $data['title'] = 'kelurahan';
            $data['page'] = 'kelurahan';
            $data['data'] = Villages::getVillages();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kelurahan_create';
            $data['title'] = 'Add kelurahan';
            $data['page'] = 'kelurahan';
            $data['user_id'] = Auth::User()->id;
            $data['district'] = Districts::orderBy('name', 'asc')->get();
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $kelurahan = new Villages;
            $kelurahan->name = strtoupper($request->name);
            $kelurahan->district_id =$request->district_id;
            $kelurahan->save();

            return redirect(route('kelurahan.index'))->with('success', 'Tambah kelurahan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kelurahan_show';
            $data['title'] = 'Detail kelurahan';
            $data['page'] = 'kelurahan';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Villages::where('id',$id)->first();
            $data['district'] = Districts::All();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kelurahan_edit';
            $data['title'] = 'Edit kelurahan';
            $data['page'] = 'kelurahan';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Villages::where('id',$id)->first();
            $data['district'] = Districts::orderBy('name', 'asc')->get();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $kelurahan = Villages::where('id',$id)->first();
            $kelurahan->name = strtoupper($request->name);
            $kelurahan->district_id =$request->district_id;
            $kelurahan->update();
            return redirect(route('kelurahan.index'))->with('success', 'Update kelurahan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelurahan = Villages::where('id', $id);
        $kelurahan->delete();
        return redirect(route('kelurahan.index'))->with('success', 'Delete kelurahan Berhasil!');
    }
}
