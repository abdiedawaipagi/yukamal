<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\MosqueEvent;
use App\Model\Mosque;
use App\Model\MosqueDkmUser;
use App\Image_uploaded;
use Image;
use File;

class Event extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    public function index()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.event';
        $data['title'] = 'Event Masjid/Musholla';
        $data['page'] = 'event';
        if ($data['role'] == 'admin') {
            $data['data'] = MosqueEvent::getEvent();
        } else {
            $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
            $data['data'] = MosqueEvent::getEventDkm($data['MosqueDkmUser']->mosque_id);
        }
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.event_create';
        $data['title'] = 'Add Event Masjid/Musholla';
        $data['page'] = 'event';
        $data['masjid'] = Mosque::all();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $event = new MosqueEvent;
            $event->date = date('Y-m-d', strtotime($request->date));
            $event->name = $request->name;
            $event->description = $request->description;
            $event->mosque_id = $request->mosque_id;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);
                
                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'event_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                $event->pic = $fileName;
            }

            $event->save();

            return redirect('admin/event')->with('success', 'Daftar Acara Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.event_show';
        $data['title'] = 'Detail Event Masjid/Musholla';
        $data['page'] = 'event';
        $data['data'] = MosqueEvent::find($id);
        $data['masjid'] = Mosque::All();
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        $data['view'] = 'backend.admin.event_edit';
        $data['title'] = 'Edit Event Masjid/Musholla';
        $data['page'] = 'event';
        $data['data'] = MosqueEvent::find($id);
        $data['masjid'] = Mosque::All();
        $data['MosqueDkmUser'] = MosqueDkmUser::where('user_id',Auth::user()->id)->first();
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $event = MosqueEvent::where('id',$id)->first();
            $event->date = date('Y-m-d', strtotime($request->date));
            $event->name = $request->name;
            $event->description = $request->description;
            $event->mosque_id = $request->mosque_id;

            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpg,png,jpeg'
                ]);

                //JIKA FOLDERNYA BELUM ADA
                if (!File::isDirectory($this->path)) {
                    //MAKA FOLDER TERSEBUT AKAN DIBUAT
                    File::makeDirectory($this->path);
                }
                //MENGAMBIL FILE IMAGE DARI FORM
                $file = $request->file('image');
                //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                $fileName = 'event_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                Image::make($file)->save($this->path . '/' . $fileName);

                File::delete($this->path.'/'.$request->image_old);

                $event->pic = $fileName;
            }else{
                if (empty($request->image_old)) {
                    $event->pic = $request->image;

                    File::delete($this->path.'/'.$request->image_name);
                }
            }

            $event->update();

            return redirect('admin/event')->with('success', 'Update Acara Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $event = MosqueEvent::find($id);
        File::delete($this->path.'/'.$event->pic);
        $event->delete();
        return redirect(route('event.index'))->with('success', 'Delete Acara Berhasil!');
    }
}
