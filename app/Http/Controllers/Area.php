<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Regencies;
use App\Model\Districts;
use App\Model\Villages;
use DataTables;
use DB;

class Area extends Controller
{
    /**
     * Menampilkan data kota.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCity($province_id)
    {
        $city = Regencies::where('province_id', '=', $province_id)->orderBy('name', 'asc')->get();
        return json_encode($city);
    }

    /**
     * Menampilkan data kecamatan.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDisctricts($regency_id)
    {
        $disctricts = Districts::where('regency_id', '=', $regency_id)->orderBy('name', 'asc')->get();
        return json_encode($disctricts);
    }

    public static function getAllDisctricts(Request $req)
    {
        // dd($req->all());
        $districts = DB::table('districts as d')->leftJoin('regencies as r','d.regency_id','r.id')->select('d.id','d.name as name','r.name as regency_name');

        $posts = DataTables::of($districts->get())->make(true);
        $post = json_decode(json_encode($posts),true)['original'];
        $data = [];

        foreach ($post['data'] as $p) {
            $insert['name'] = $p['name'];
            $insert['regency_name'] = $p['regency_name'];
            $insert['actions'] = '
                <a href="'.route('kecamatan.show', $p['id']).'"><i class="fa fa-eye"></i></a> |
                <a href="'.route('kecamatan.edit', $p['id']).'"><i class="fa fa-pencil"></i></a> |
                <a data-target="#static-'.$p['id'].'" id="modal_delete" data-id="'.$p['id'].'" data-toggle="modal"><i class="fa fa-trash"></i></a>
                <div id="static-'.$p['id'].'" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form method="post" action="'.route('kecamatan.destroy', $p['id']).'">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input type="hidden" name="_method" value="DELETE">
                    
                    <div class="modal-body">
                        <p> Apakah Anda yakin ini menghapus? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                        <button type="submit"  class="btn red">Delete</button>
                    </div>
                    </form>
                </div>
            ';

            $data[] = $insert;
        }

        $jsonData = array(
            "draw" => $post['draw'],
            "recordsTotal" => $post['recordsTotal'],
            "recordsFiltered" => $post['recordsFiltered'],
            "data" => $data
        );

        echo json_encode($jsonData);
    }

    public static function getAllVillages(Request $req)
    {
        $villages = DB::table('villages as v')->join('districts as d','v.district_id','d.id')->select('v.*','d.name as district_name');

        $posts = DataTables::of($villages->get())->make(true);
        $post = json_decode(json_encode($posts),true)['original'];
        $data = [];

        foreach ($post['data'] as $p) {
            $insert['name'] = $p['name'];
            $insert['district_name'] = $p['district_name'];
            $insert['actions'] = '
                <a href="'.route('kelurahan.show', $p['id']).'"><i class="fa fa-eye"></i></a> |
                <a href="'.route('kelurahan.edit', $p['id']).'"><i class="fa fa-pencil"></i></a> |
                <a data-target="#static-'.$p['id'].'" id="modal_delete" data-id="'.$p['id'].'" data-toggle="modal"><i class="fa fa-trash"></i></a>
                <div id="static-'.$p['id'].'" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form method="post" action="'.route('kelurahan.destroy', $p['id']).'">
                    <input type="hidden" name="_token" value="'.csrf_token().'">
                    <input type="hidden" name="_method" value="DELETE">
                    
                    <div class="modal-body">
                        <p> Apakah Anda yakin ini menghapus? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                        <button type="submit"  class="btn red">Delete</button>
                    </div>
                    </form>
                </div>
            ';

            $data[] = $insert;
        }

        $jsonData = array(
            "draw" => $post['draw'],
            "recordsTotal" => $post['recordsTotal'],
            "recordsFiltered" => $post['recordsFiltered'],
            "data" => $data
        );

        echo json_encode($jsonData);
    }

    /**
     * Menampilkan data kecamatan.
     *
     * @return \Illuminate\Http\Response
     */
    public function getVillages($district_id)
    {
        $villages = Villages::where('district_id', '=', $district_id)->orderBy('name', 'asc')->get();
        return json_encode($villages);
    }
}