<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Districts;
use App\Model\Regencies;

class Kecamatan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        if ($role == 'admin') {
            $data['view'] = 'backend.admin.kecamatan';
            $data['title'] = 'Kecamatan';
            $data['page'] = 'kecamatan';
            $data['data'] = Districts::getDisctricts();
            $data['role'] = $role;
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kecamatan_create';
            $data['title'] = 'Add Kecamatan';
            $data['page'] = 'kecamatan';
            $data['user_id'] = Auth::User()->id;
            $data['regency'] = Regencies::All();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $kecamatan = new Districts;
            $kecamatan->name = strtoupper($request->name);
            $kecamatan->regency_id =$request->regency_id;
            $kecamatan->save();

            return redirect(route('kecamatan.index'))->with('success', 'Tambah kecamatan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kecamatan_show';
            $data['title'] = 'Detail Kecamatan';
            $data['page'] = 'kecamatan';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Districts::where('id',$id)->first();
            $data['regency'] = Regencies::All();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::user()->role;
        if ($data['role'] == 'admin') {
            $data['view'] = 'backend.admin.kecamatan_edit';
            $data['title'] = 'Edit Kecamatan';
            $data['page'] = 'kecamatan';
            $data['user_id'] = Auth::User()->id;
            $data['data'] = Districts::where('id',$id)->first();
            $data['regency'] = Regencies::All();
            return view('layout.backend.app', $data);
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $kecamatan = Districts::where('id',$id)->first();
            $kecamatan->name = strtoupper($request->name);
            $kecamatan->regency_id =$request->regency_id;
            $kecamatan->update();
            return redirect(route('kecamatan.index'))->with('success', 'Update kecamatan Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kecamatan = Districts::where('id', $id);
        $kecamatan->delete();
        return redirect(route('kecamatan.index'))->with('success', 'Delete kecamatan Berhasil!');
    }
}
