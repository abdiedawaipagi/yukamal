<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Model\UserProfile;
use App\User;
use App\Image_uploaded;
use Image;
use File;

class Profil extends Controller
{
    public $path;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = public_path('uploads');
    }

    public function index()
    {
        return redirect('admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::guest()) {
            return redirect('/');
        }
        $data['role'] = Auth::User()->role;
        $data['view'] = 'backend.admin.profile_show';
        $data['title'] = 'Edit profil';
        $data['page'] = 'profil';
        $data['user_id'] = Auth::User()->id;
        $data['data'] = UserProfile::getProfile($data['user_id']);
        return view('layout.backend.app', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Auth::User()->role;
        $data['view'] = 'backend.admin.profile_edit';
        $data['title'] = 'Edit profil';
        $data['page'] = 'profil';
        $data['user_id'] = Auth::User()->id;
        $data['data'] = UserProfile::getProfile(Auth::User()->id);
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $profil = UserProfile::where('user_id',$id)->first();

            if (is_null($profil)) {
                $profil = new UserProfile;
                $profil->user_id = $id;
                $profil->first_name = $request->first_name;
                $profil->last_name = $request->last_name;
                $profil->gender = $request->gender;
                $profil->dob = date('Y-m-d', strtotime($request->dob));

                if (isset($request->image)) {
                    $this->validate($request, [
                        'image' => 'required|image|mimes:jpg,png,jpeg'
                    ]);
                    
                    //JIKA FOLDERNYA BELUM ADA
                    if (!File::isDirectory($this->path)) {
                        //MAKA FOLDER TERSEBUT AKAN DIBUAT
                        File::makeDirectory($this->path);
                    }
                    //MENGAMBIL FILE IMAGE DARI FORM
                    $file = $request->file('image');
                    //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                    $fileName = 'profile_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                    //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                    Image::make($file)->save($this->path . '/' . $fileName);

                    File::delete($this->path.'/'.$request->image_old);

                    $profil->pic = $fileName;
                }else{
                    if (empty($request->image_old)) {
                        $profil->pic = $request->image;

                        File::delete($this->path.'/'.$request->image_name);
                    }
                }

                $profil->save();
            } else {
                $profil->first_name = $request->first_name;
                $profil->last_name = $request->last_name;
                $profil->gender = $request->gender;
                $profil->dob = date('Y-m-d', strtotime($request->dob));

                if (isset($request->image)) {
                    $this->validate($request, [
                        'image' => 'required|image|mimes:jpg,png,jpeg'
                    ]);
                    
                    //JIKA FOLDERNYA BELUM ADA
                    if (!File::isDirectory($this->path)) {
                        //MAKA FOLDER TERSEBUT AKAN DIBUAT
                        File::makeDirectory($this->path);
                    }
                    //MENGAMBIL FILE IMAGE DARI FORM
                    $file = $request->file('image');
                    //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
                    $fileName = 'profile_'.uniqid().'_'.date('ymdHis').'.' . $file->getClientOriginalExtension();
                    //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
                    Image::make($file)->save($this->path . '/' . $fileName);

                    File::delete($this->path.'/'.$request->image_old);

                    $profil->pic = $fileName;
                }else{
                    if (empty($request->image_old)) {
                        $profil->pic = $request->image;

                        File::delete($this->path.'/'.$request->image_name);
                    }
                }

                $profil->update();
            }

            $user = User::where('id',$id)->first();
            $user->email = $request->email;
            $user->update();

            return redirect(route('profilpengguna.show', Auth::user()->id))->with('success', 'Update profil Berhasil!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password($id)
    {
        $data['role'] = Auth::User()->role;
        $data['view'] = 'backend.admin.profile_password';
        $data['title'] = 'Edit Kata Sandi';
        $data['page'] = 'password';
        $data['data'] = UserProfile::getProfile(Auth::User()->id);
        return view('layout.backend.app', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request, $id)
    {
        $pass = Auth::user()->password;
        $checkCurrentPassword = Hash::check($request->old_password, $pass);
        $newPassword = bcrypt($request->new_password);

        if ($checkCurrentPassword) {
            if ($request->old_password !== $request->new_password) {
                if ($request->new_password === $request->confirm_new_password) {
                    $user = User::where('id',$id)->first();
                    $user->password = $newPassword; 
                    $user->update(); 
                    return redirect(route('profilpengguna.password', Auth::user()->id))->with('success', 'Ubah Kata sandi Berhasil!');
                }else{
                    return redirect(route('profilpengguna.password', Auth::user()->id))->with('alert', 'Kata sandi baru tidak sama dengan Konfirmasi kata sandi baru!');
                }
            }else{
                return redirect(route('profilpengguna.password', Auth::user()->id))->with('alert', 'Kata sandi baru tidak boleh sama dengan password lama!');
            }
        }else{
            return redirect(route('profilpengguna.password', Auth::user()->id))->with('alert', 'Kata sandi lama tidak sesuai!');
        }
    }
}
