        </div>
        <!-- END CONTAINER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2016 &copy; Metronic Theme By
                    <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{url('/')}}/js/backend/jquery.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <script src="{{url('/')}}/js/backend/datatable.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/datatables.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap-table.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/toastr.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/jquery.repeater.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/table-datatables-managed.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/bootstrap-modal.js" type="text/javascript"></script>
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{url('/')}}/js/backend/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <script src="{{url('/')}}/js/backend/ui-extended-modals.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/form-repeater.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/ui-toastr.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{url('/')}}/js/backend/table-datatables-responsive.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/backend/layout.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>
</html>