<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            @if($role == 'admin')
            <li class="nav-item start {{($page == 'dashboard' ? 'active' : '')}}">
                <a href="{{ route('admin') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Home</span>
                    <span class="selected"></span>
                </a>
            </li>
            @endif
            <li class="nav-item {{($page == 'profil' || $page == 'password' ? 'active' : '')}}">
                <a href="{{ route('profilpengguna.show', Auth::user()->id) }}">
                    <i class="fa fa-user"></i>
                    <span class="title">Profil Pengguna</span>
                    <span class="selected"></span>
                </a>
            </li>
            @if($role == 'admin')
            <li class="nav-item {{($page == 'provinsi' || $page == 'kota' || $page == 'kecamatan' || $page == 'kelurahan' || $page == 'kodepos' || $page == 'category' || $page == 'sub-category' || $page == 'pembangunan' ? 'active' : '')}}">
                <a href="{{ route('admin') }}" class="nav-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Master Data</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{($page == 'provinsi' || $page == 'kota' || $page == 'kecamatan' || $page == 'kelurahan' || $page == 'kodepos' ? 'active' : '')}}">
                        <a href="#" class="nav-link nav-toggle">
                            <i class="fa fa-globe"></i>
                            <span class="title">Wilayah</span>
                            <span class="selected"></span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item start {{($page == 'provinsi' ? 'active' : '')}}">
                                <a href="{{ route('provinsi.index') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-globe"></i>
                                    <span class="title">Provinsi</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item start {{($page == 'kota' ? 'active' : '')}}">
                                <a href="{{ route('kota.index') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-globe"></i>
                                    <span class="title">Kota</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item start {{($page == 'kecamatan' ? 'active' : '')}}">
                                <a href="{{ route('kecamatan.index') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-globe"></i>
                                    <span class="title">Kecamatan</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item start {{($page == 'kelurahan' ? 'active' : '')}}">
                                <a href="{{ route('kelurahan.index') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-globe"></i>
                                    <span class="title">Kelurahan</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{($page == 'category' || $page == 'sub-category' ? 'active' : '')}}">
                        <a href="" class="nav-link nav-toggle">
                            <i class="fa fa-money"></i>
                            <span class="title">Keuangan</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item {{($page == 'category' ? 'active' : '')}}">
                                <a href="{{ route('category.index') }}" class="nav-link ">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Kategori</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item {{($page == 'sub-category' ? 'active' : '')}}">
                                <a href="{{ route('subcategory.index') }}" class="nav-link ">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Sub Kategori</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{($page == 'pembangunan'? 'active' : '')}}">
                        <a href="" class="nav-link nav-toggle">
                            <i class="fa fa-industry"></i>
                            <span class="title">Pembangunan</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item {{($page == 'pembangunan' ? 'active' : '')}}">
                                <a href="{{ route('pembangunan.index') }}" class="nav-link ">
                                    <i class="fa fa-industry"></i>
                                    <span class="title">Bobot Pembangunan</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            @endif
            <li class="nav-item {{($page == 'informasi' || $page == 'sejarah' || $page == 'visi-misi' || $page == 'dkm' || $page == 'data-jamaah' || $page == 'keuangan' || $page == 'pembangunan-masjid' || $page == 'galeri' || $page == 'event' ? 'active' : '')}}">
                <a href="{{ route('admin') }}" class="nav-link nav-toggle">
                    <i class="fa fa-moon-o"></i>
                    <span class="title">Data Masjid</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{($page == 'informasi' ? 'active' : '')}}">
                        <a href="{{ route('masjid') }}" class="nav-link nav-toggle">
                            <i class="fa fa-info-circle"></i>
                            <span class="title">Informasi</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'sejarah' ? 'active' : '')}}">
                        <a href="{{ route('sejarah.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-history"></i>
                            <span class="title">Sejarah</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'visi-misi' ? 'active' : '')}}">
                        <a href="{{ route('visimisi.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-rocket"></i>
                            <span class="title">Visi-Misi</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'dkm' ? 'active' : '')}}">
                        <a href="{{ route('dkm.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-users"></i>
                            <span class="title">Pengurus</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'data-jamaah' ? 'active' : '')}}">
                        <a href="{{ route('masjid.jamaah') }}" class="nav-link nav-toggle">
                            <i class="fa fa-users"></i>
                            <span class="title">Jama'ah</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'keuangan' ? 'active' : '')}}">
                        <a href="{{ route('keuangan.index') }}" class="nav-link nav-toggle">
                            <i class="icon-home"></i>
                            <span class="title">Keuangan</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'pembangunan-masjid' ? 'active' : '')}}">
                        <a href="{{ route('pembangunan_masjid.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-industry"></i>
                            <span class="title">Pembangunan</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'galeri' ? 'active' : '')}}">
                        <a href="{{ route('galeri.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-picture-o"></i>
                            <span class="title">Galeri</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'event' ? 'active' : '')}}">
                        <a href="{{ route('event.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-ticket"></i>
                            <span class="title">Acara</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            @if($role == 'dkm' || $role == 'admin')
            <li class="nav-item {{($page == 'donasi-masjid' || $page == 'donasi-yukamal' ? 'active' : '')}}">
                <a href="{{ route('admin') }}" class="nav-link nav-toggle">
                    <i class="fa fa-money"></i>
                    <span class="title">Donasi</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{($page == 'donasi-masjid' ? 'active' : '')}}">
                        <a href="{{ route('donasimasjid.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-moon-o"></i>
                            <span class="title">Masjid</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    @if($role == 'admin')
                    <li class="nav-item start {{($page == 'donasi-yukamal' ? 'active' : '')}}">
                        <a href="{{ route('donasiyukamal.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-star-o"></i>
                            <span class="title">YukAmal</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            @if($role == 'admin')
            <li class="nav-item {{($page == 'halaman' || $page == 'team' ? 'active' : '')}}">
                <a href="{{ route('admin') }}" class="nav-link nav-toggle">
                    <i class="fa fa-star-o"></i>
                    <span class="title">YukAmal</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{($page == 'halaman' ? 'active' : '')}}">
                        <a href="{{ route('page.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-file-o"></i>
                            <span class="title">Halaman</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'team' ? 'active' : '')}}">
                        <a href="{{ route('team.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-users"></i>
                            <span class="title">Tim Kami</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{($page == 'berita' ? 'active' : '')}}">
                        <a href="{{ route('berita.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-newspaper-o"></i>
                            <span class="title">Berita</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            @if($role == 'admin')
            <li class="nav-item {{($page == 'pengguna'? 'active' : '')}}">
                <a href="{{ route('admin') }}" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Kelola Pengguna</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{($page == 'pengguna' ? 'active' : '')}}">
                        <a href="{{ route('pengguna.index') }}" class="nav-link nav-toggle">
                            <i class="fa fa-users"></i>
                            <span class="title">Daftar Pengguna</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
</div>