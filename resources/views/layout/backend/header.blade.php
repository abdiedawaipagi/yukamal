<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>YUK | {{(!empty($title) ? $title : '')}}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Testing Master Template Laravel" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{url('/')}}/css/backend/google-font.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link href="{{url('/')}}/css/backend/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('/')}}/css/backend/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('/')}}/css/backend/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{url('/')}}/css/backend/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/backend/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <img src="{{url('/')}}/img/logo.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> {{ Auth::user()->username }} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            <i class="icon-key"></i> {{ __('Logout') }} </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">