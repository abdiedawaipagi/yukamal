<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Yukamal</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/linearicons.css">
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/bootstrap.css">
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/magnific-popup.css">
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/jquery-ui.css">				
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/nice-select.css">							
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/animate.min.css">
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/owl.carousel.css">				
		<link rel="stylesheet" href="{{url('/')}}/frontend/css/main.css">
		<link rel="stylesheet" href="{{url('/')}}/frontend/slick/slick.css">
		<link rel="stylesheet" href="{{url('/')}}/frontend/slick/slick-theme.css">
		<!-- js -->
		<script src="{{url('/')}}/js/backend/jquery.min.js" type="text/javascript"></script>
		<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
		<script src="{{url('/')}}/frontend/js/popper.min.js"></script>
		<script src="{{url('/')}}/frontend/js/vendor/bootstrap.min.js"></script>
		<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl5cuWr96CfdeXezgzmToFgf_7Mv8RDmg&callback=initMap" async defer></script> -->	
		<script src="{{url('/')}}/frontend/js/easing.min.js"></script>			
		<script src="{{url('/')}}/frontend/js/hoverIntent.js"></script>
		<script src="{{url('/')}}/frontend/js/superfish.min.js"></script>	
		<script src="{{url('/')}}/frontend/js/jquery.ajaxchimp.min.js"></script>
		<script src="{{url('/')}}/frontend/js/jquery.magnific-popup.min.js"></script>	
		<script src="{{url('/')}}/frontend/js/jquery.tabs.min.js"></script>						
		<script src="{{url('/')}}/frontend/js/jquery.nice-select.min.js"></script>	
        <script src="{{url('/')}}/frontend/js/isotope.pkgd.min.js"></script>			
		<script src="{{url('/')}}/frontend/js/waypoints.min.js"></script>
		<script src="{{url('/')}}/frontend/js/jquery.counterup.min.js"></script>
		<script src="{{url('/')}}/frontend/js/simple-skillbar.js"></script>							
		<script src="{{url('/')}}/frontend/js/owl.carousel.min.js"></script>							
		<script src="{{url('/')}}/frontend/js/mail-script.js"></script>	
		<script src="{{url('/')}}/frontend/js/main.js"></script>
		<script src="{{url('/')}}/js/backend/jquery.repeater.js" type="text/javascript"></script>
		<script src="{{url('/')}}/js/backend/app.min.js" type="text/javascript"></script>
		<script src="{{url('/')}}/js/backend/form-repeater.min.js" type="text/javascript"></script>
		<script src="{{url('/')}}/frontend/js/jquery.jscroll.min.js"></script>
		<script src="{{url('/')}}/frontend/slick/slick.js"></script>
		<script src="{{url('/')}}/frontend/slick/slick.min.js"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

		<!-- end js -->
		<style type="text/css">
			#total_trans {
	            background-color: #f2b380;
	            color: white;
	            padding: 1px 3px;
	            border-radius: 50%;
	        }
	        .pagination {
	        	margin-top: 5em;
	        	justify-content: center;
	        }
		</style>
	</head>
	<body>	
	  <header id="header">
	    <div class="main-menu" style="margin: 0 3em">
	    	<div class="row align-items-center justify-content-between d-flex">
		      <div id="logo">
		        <a href="{{ url('/') }}" style="font-size: 1.5em;font-weight: bold;color: #44c19a">YUKAMAL</a>
		      </div>
		      <nav id="nav-menu-container">
		        <ul class="nav-menu">
		          <li><a href="{{ url('/') }}">Beranda</a></li>
		          <li><a href="{{ route('donasi') }}">Donasi</a></li>
		          <li><a href="{{ route('acara') }}">Acara</a></li>
		          <li><a href="{{ route('berita') }}">Berita</a></li>
		          <li><a href="{{ route('about') }}">Tentang Kami</a></li>
		          @guest
		          <li class="menu-has-children"><a href="#">Masuk</a>
		            <ul style="width: 300px;">
		            	<form method="post" action="{{ route('login') }}">
		            		{{ csrf_field() }}
							<li style="padding: .5em">
								<input style="padding: .2em .5em;" type="text" name="email" placeholder="Username" class="form-control">
							</li>
							<li style="padding: .5em">
								<input style="padding: .2em .5em;" type="password" name="password" placeholder="Password" class="form-control">
							</li>
							<li style="padding: .5em">
								<button class="primary-btn text-uppercase btn-block">Masuk</button>
							</li>
							<li style="text-align: center;">
								<span><a href="{{route('forgot_password')}}">Forgot Password</a></span>
							</li>
							<li style="padding: 1em"><p style="text-align: center">Atau</p></li>
							<li style="padding: .5em">
								<a href="{{ url('/auth/facebook') }}" style="background-color: #4e4eff;color: white;text-align: center" class="primary-btn text-uppercase btn-block">Masuk Dengan Facebook</a>
							</li>
							<!-- <li style="padding: .5em">
								<a style="background-color: #fb5151;color: white;text-align: center" class="primary-btn text-uppercase btn-block">Masuk Dengan Google</a>
							</li> -->
							<li style="text-align: center;">
								<span><a href="{{route('register_user')}}">Tidak Punya Akun? Register</a></span>
							</li>
		            	</form>
		            </ul>
		          </li>	
		          @else
		           <li class="menu-has-children">
                        <a style="color: black;">
                        	@if($user->pic != null)
                            <span>{{ Auth::user()->username }} <img style="width: 25px;margin-left: .5em" class="rounded-circle" src="{{ $user->pic }}"></span>
                        	@else
                        	<span>{{ Auth::user()->username }} <img style="width: 25px;margin-left: .5em" class="rounded-circle" src="{{ url('/') }}/uploads/default.png"></span>
                        	@endif
                        </a>

                        <ul style="width: 150px;padding-left: 1em;">
                            <li>
                            	<a href="{{ route('profile') }}">Profil</a>
                            </li>
                            <li>
                            	<a href="{{ route('mytransaction') }}">Transaksi Saya <span id="total_trans">{{$total_trans->total}} </span></a>
                            </li>
                            <li>
                                <a href="#"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Keluar
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
		           @endguest
		        </ul>
		      </nav><!-- #nav-menu-container -->		    		
	    	</div>
	    </div>
	  </header><!-- #header -->