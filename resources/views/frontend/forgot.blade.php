<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8" style="margin-top: 10%;">
            <div class="panel panel-default">
                <h2 style="text-align: center;margin-bottom: 1em;">Forgot Password</h2>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('proses_password') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Email</label>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="user_email" required autofocus>
                                <input type="hidden" class="form-control" name="role" value="member">
                            </div>
                        </div>

                        <div class="form-group" id="password">
                            <label for="password" class="col-md-4 control-label">New Password</label>
                            <div class="col-md-12">
                                <input type="password" class="form-control" name="new_password" required autofocus>
                                <input type="hidden" class="form-control" name="role" value="member">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Next
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{url('/')}}/frontend/js/vendor/jquery-2.2.4.min.js"></script>
<script type="text/javascript">
    $('#password').hide();
    $('#email').on('change',function(){
        $('#password').show();
    })
</script>