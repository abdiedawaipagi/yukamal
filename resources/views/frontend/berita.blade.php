<section class="portfolio-area section-gap" id="portfolio">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content">
                <div class="title text-center">
                    <h1 class="mb-10">Berita</h1>
                    <p>Berita seputar kegiatan masjid</p>
                </div>
            </div>
        </div>
        
        <div class="filters-content">
            <div class="infinite-scroll">
                <div class="row">
                	@foreach($berita as $val)
                	<div class="col-md-4">
                		<div class="card" style="margin-top: 1em;">
                    		<img style="width: 100%;height: 180px;" src="{{ url('/uploads/'.$val->pic) }}">
                    		<div class="card-title" style="padding: 1em 1em 0 1em;margin-bottom: -.5em">
                    			<h6>{{$val->title}}</h6>
                                <p><?= str_limit($val->description, $limit = 30, $end = '...')?></p>
                    		</div>
                    		<div class="card-body" style="padding: 1em 1em 0 1em;">
                    			<div class="row">
                    				<a href="{{ route('detailberita', $val->id)}}" class="primary-btn text-uppercase btn-block text-center">See More >></a>
                    			</div>
                    		</div>
                    	</div>
                	</div>
                	@endforeach
                </div>
                {{ $berita->links() }}
            </div>
        </div>
        
    </div>
</section>
<!-- End portfolio-area Area -->	

<!-- Start brands Area -->
<script src="{{url('/')}}/frontend/js/vendor/jquery-2.2.4.min.js"></script>
<script type="text/javascript">
	$("#provinsi").change(function(){
        var province_id = $("#provinsi").val();
        var my_url = "<?= url('/city')?>"+'/'+province_id;
        
        if (province_id == '') {
            $('#kota').html('');
            $('#kecamatan').html('');
            $('#kelurahan').html('');
        } else {
            $.ajax({
                url: my_url,
                dataType: 'json',
                success: function (data) {
                	console.log(data)
                    $('#kota').prop('disabled', false);
                    $('#kota').html('');
                    $('#kota').append('<option value=""></option>');
                    data.forEach(function(item, index){
                        $('#kota').append('<option value="'+item.id+'">'+item.name+'</option>');
                    });
                }
            });
        }
    });
    $("#kota").change(function(){
        var city_id = $("#kota").val();
        var my_url = "<?= url('/districts')?>"+'/'+city_id;

        $.ajax({
            url: my_url,
            dataType: 'json',
            success: function (data) {
                $('#kecamatan').prop('disabled', false);
                $('#kecamatan').html('');
                $('#kecamatan').append('<option value=""></option>');
                data.forEach(function(item, index){
                    $('#kecamatan').append('<option value="'+item.id+'">'+item.name+'</option>');
                });
            }
        });
    });
</script>