<section class="section-gap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div>
					<span>Tanggal Acara : {{$acara->date}}</span>
					<h2>{{$acara->name}}</h2>
					<div>
						<img style="width: 100%;height: 180px;" src="{{ url('/uploads/'.$acara->pic) }}">
					</div>
					<div>
						<p>
							Keterangan : <?= $acara->description ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>