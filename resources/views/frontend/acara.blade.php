<section class="banner-area">
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-between">
        <div style="position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: #ababab8f;height: 550px;"></div>
        </div>
    </div>                  
</section>
<section class="portfolio-area section-gap" id="portfolio">
    <div class="container">
        <div class="filters-content">
            <form class="form-horizontal" method="POST" action="{{ route('acara-search') }}">
                <div class="row" style="margin-bottom: 2em;">
                    {{ csrf_field() }}
                	<div class="col-md-8">
                		<input type="text" name="search" class="form-control" placeholder="Search Event">
                	</div>
                	<div class="col-md-4">
                		<button type="submit" class="btn btn-primary btn-block">Search</button>
                	</div>
                </div>
            </form>
            <div class="infinite-scroll">
                <div class="row">
                	@foreach($acara as $val)
                	<div class="col-md-4">
                		<div class="card" style="margin-top: 1em;">
                    		<img style="width: 100%;height: 180px;" src="{{ url('/uploads/'.$val->pic) }}">
                    		<div class="card-title" style="padding: 1em 1em 0 1em;margin-bottom: .5em">
                    			<h6>{{$val->name}}</h6>
                    		</div>
                    		<hr style="margin:0;">
                    		<div class="card-body" style="padding: 1em 1em 0 1em;">
                    			<div>
                    				<p>{{$val->date}}</p>
                    			</div>
                    			<div>
                    				<p>{{$val->mosque}}</p>
                    			</div>
                    			<div class="row">
                    				<a href="{{ route('detailacara', $val->id)}}" class="primary-btn text-uppercase btn-block text-center">Detail Acara >></a>
                    			</div>
                    		</div>
                    	</div>
                	</div>
                	@endforeach
                </div>
                {{ $acara->links() }}
            </div>
        </div>
        
    </div>
</section>
<!-- End portfolio-area Area -->	

<!-- Start brands Area -->
<script src="{{url('/')}}/frontend/js/vendor/jquery-2.2.4.min.js"></script>
<script type="text/javascript">
	$("#provinsi").change(function(){
        var province_id = $("#provinsi").val();
        var my_url = "<?= url('/city')?>"+'/'+province_id;
        
        if (province_id == '') {
            $('#kota').html('');
            $('#kecamatan').html('');
            $('#kelurahan').html('');
        } else {
            $.ajax({
                url: my_url,
                dataType: 'json',
                success: function (data) {
                	console.log(data)
                    $('#kota').prop('disabled', false);
                    $('#kota').html('');
                    $('#kota').append('<option value=""></option>');
                    data.forEach(function(item, index){
                        $('#kota').append('<option value="'+item.id+'">'+item.name+'</option>');
                    });
                }
            });
        }
    });
</script>