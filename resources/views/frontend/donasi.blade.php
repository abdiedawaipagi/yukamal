<section class="portfolio-area section-gap" id="portfolio">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content">
                <div class="title text-center">
                    <h1 class="mb-10">#YUKDONASI</h1>
                    <p>Donasikan uang anda untuk pembangunan masjid</p>
                </div>
            </div>
        </div>
        
        <div class="filters-content">
            <form class="form-horizontal" method="POST" action="{{ route('donasi-search') }}">
                {{ csrf_field() }}
                <div class="row" style="margin-bottom: 2em;">
                    <div class="col-md-2">
                        <select class="form-control" id="provinsi" name="provinsi">
                            <option>Provinsi</option>
                            @foreach($provinces as $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" id="kota" name="kota">
                            <option>Kota</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" id="kecamatan" name="kecamatan">
                            <option value="0">Kecamatan</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="search" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                    </div>
                </div>
            </form>
            <div class="">
                <div class="row">
                    @foreach($masjid as $val)
                    <div class="col-md-4">
                        <div class="card" style="margin-top: 1em">
                            <img style="width: 100%;height: 180px;" src="{{ url('/uploads/'.$val->pic) }}">
                            <div class="card-title" style="padding: 1em 1em 0 1em;margin-bottom: -.5em">
                                <h6>{{$val->name}}</h6>
                                <p><?= str_limit($val->address, $limit = 30, $end = '...')?></p>
                            </div>
                            <hr style="margin:0;">
                            <div class="card-body" style="padding: 1em 1em 0 1em;">
                                <div class="row" style="font-size: .8em;text-align: center;">
                                    <div class="col-md-6"><p>Donasi Terkumpul</p></div>
                                    <div class="col-md-6"><p>Tahap Pembangunan</p></div>
                                </div>
                                <div class="row" style="text-align: center;">
                                    <div class="col-md-6">
                                        <p>Rp {{number_format($val->total) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>60 %</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <a href="{{ route('detail', $val->id)}}" class="primary-btn text-uppercase btn-block text-center">Donasi Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div style="text-align: center">
                    {{ $masjid->links( "pagination::bootstrap-4") }}
                </div>
            </div>
        </div>
        
    </div>
</section>
<!-- End portfolio-area Area -->    

<!-- Start brands Area -->
<script type="text/javascript">
    $("#provinsi").change(function(){
        var province_id = $("#provinsi").val();
        var my_url = "<?= url('/city')?>"+'/'+province_id;
        
        if (province_id == '') {
            $('#kota').html('');
            $('#kecamatan').html('');
            $('#kelurahan').html('');
        } else {
            $.ajax({
                url: my_url,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#kota').prop('disabled', false);
                    $('#kota').html('');
                    $('#kota').append('<option value=""></option>');
                    data.forEach(function(item, index){
                        $('#kota').append('<option value="'+item.id+'">'+item.name+'</option>');
                    });
                }
            });
        }
    });
    $("#kota").change(function(){
        var city_id = $("#kota").val();
        var my_url = "<?= url('/districts')?>"+'/'+city_id;

        $.ajax({
            url: my_url,
            dataType: 'json',
            success: function (data) {
                $('#kecamatan').prop('disabled', false);
                $('#kecamatan').html('');
                $('#kecamatan').append('<option value=""></option>');
                data.forEach(function(item, index){
                    $('#kecamatan').append('<option value="'+item.id+'">'+item.name+'</option>');
                });
            }
        });
    });
</script>