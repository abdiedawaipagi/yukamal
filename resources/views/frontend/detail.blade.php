<style type="text/css">
	.table td {
		border: none !important;
	}
	.content-relative {
	  position: relative;
	  width: 100%;
	}

	.thumb {
	  opacity: 1;
	  display: block;
	  width: 100%;
	  height: 200px;
	  transition: .5s ease;
	  backface-visibility: hidden;
	}

	.middle {
	  transition: .5s ease;
	  opacity: 0;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  -ms-transform: translate(-50%, -50%);
	  text-align: center;
	}

	.content-relative:hover .thumb {
	  opacity: 0.3;
	}

	.content-relative:hover .middle {
	  opacity: 1;
	}

	.text {
	  background-color: #4CAF50;
	  color: white;
	  font-size: 16px;
	  padding: 16px 32px;
	}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<section class="section-gap" style="padding-bottom: 2em">
	<div class="container">
		<div class="detail-title" style="margin-bottom: 2em;">
			<h1>{{$masjid->name}}</h1>
		</div>
		<div class="row">
			<div class="col-md-6">
	  		<div class="img-detail">
	  			<img style="width: 100%" src="{{ url('/uploads/'.$masjid->pic) }}">
	  		</div>
			</div>
			<div class="col-md-6">
				<div id="terkumpul" style="padding-bottom: 2em;">
					<h1>Rp {{number_format($total_donasi->total)}}</h1>
				</div>
				<div class="progress">
				<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$total_donasi->total/$masjid->estimate*100}};background-color: #44c19a;">
				</div>
			</div>
				<div style="font-size: 20px;padding: 1em 0">
					<span style="float: left">Terkumpul dari</span>
					<span style="float: right">Rp {{number_format($masjid->estimate)}}</span>
				</div>
				<br>
				<div>
					<span style="float: left">{{$total_donasi->total/$masjid->estimate*100}} tercapai</span>
					<span style="float: right">{{$masjid->est_date}} hari lagi</span>
				</div>
				<div style="margin-top: 3em;">
					@guest
					<a href="{{route('login')}}" class="primary-btn text-uppercase text-center btn-block">Yuk Donasi</a>
					@else
					<a href="{{route('proses',$masjid->id)}}" class="primary-btn text-uppercase text-center btn-block">Yuk Donasi</a>
					@endguest
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-gap" style="padding-top: 0">
	<div class="container">
	<hr>
		<nav>
			<div class="nav nav-tabs" id="nav-tab" role="tablist">
				<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-info" role="tab" aria-controls="nav-info" aria-selected="true">Info</a>
				<a class="nav-item nav-link" id="nav-sejarah-tab" data-toggle="tab" href="#nav-sejarah" role="tab" aria-controls="nav-sejarah" aria-selected="false">Sejarah</a>
				<a class="nav-item nav-link" id="nav-vm-tab" data-toggle="tab" href="#nav-vm" role="tab" aria-controls="nav-vm" aria-selected="false">Visi & Misi</a>
				<a class="nav-item nav-link" id="nav-pengurus-tab" data-toggle="tab" href="#nav-pengurus" role="tab" aria-controls="nav-pengurus" aria-selected="false">Pengurus</a>
				<a class="nav-item nav-link" id="nav-jamaah-tab" data-toggle="tab" href="#nav-jamaah" role="tab" aria-controls="nav-jamaah" aria-selected="false">Jama'ah</a>
				<a class="nav-item nav-link" id="nav-keuangan-tab" data-toggle="tab" href="#nav-keuangan" role="tab" aria-controls="nav-keuangan" aria-selected="false">Keuangan</a>
				<a class="nav-item nav-link" id="nav-pembangunan-tab" data-toggle="tab" href="#nav-pembangunan" role="tab" aria-controls="nav-pembangunan" aria-selected="false">Pembangunan</a>
				<a class="nav-item nav-link" id="nav-galeri-tab" data-toggle="tab" href="#nav-galeri" role="tab" aria-controls="nav-galeri" aria-selected="false">Galeri</a>
				<a class="nav-item nav-link" id="nav-acara-tab" data-toggle="tab" href="#nav-acara" role="tab" aria-controls="nav-acara" aria-selected="false">Acara</a>
			</div>
		</nav>
		<div class="tab-content" id="nav-tabContent" style="margin-top: 2em">
			<div class="tab-pane fade show active" id="nav-info" role="tabpanel" aria-labelledby="nav-info-tab">
				<div class="row">
					<div class="col-md-6">
						<h3>Informasi Masjid</h3>
						<table class="table" style="margin-top: 1em">
							<tr>
								<td>Nama</td>
								<td>: {{$masjid->name}}</td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>: {{$masjid->address}}</td>
							</tr>
							<tr>
								<td>Luas Tanah</td>
								<td>: {{$masjid->surface_area}}</td>
							</tr>
							<tr>
								<td>Luas Bangunan</td>
								<td>: {{$masjid->building_area}}</td>
							</tr>
							<tr>
								<td>Status Tanah</td>
								<td>: {{$masjid->los}}</td>
							</tr>
							<tr>
								<td>Tahun Berdiri</td>
								<td>: {{$masjid->since}}</td>
							</tr>
						</table>
					</div>
					<div class="col-md-6">
						<div id="map" style="width:100%;height:500px; margin-bottom: 5em;margin-top: 5em;"></div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="nav-sejarah" role="tabpanel" aria-labelledby="nav-sejarah-tab">
				<div class="col-md-12">
					<h2 style="text-align: center">Sejarah {{$masjid->name}}</h2>
					<div style="margin-top: 1em;text-align: center">
						<img src="{{ url('/uploads/'.$masjid->image) }}" style="width: 500px;height:250px;">
					</div>
					<div style="margin-top: 1em;">
						<p style="color: black"><?= $masjid->description ?></p>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="nav-vm" role="tabpanel" aria-labelledby="nav-vm-tab">
				<!-- {{$masjid->visimisi}} -->
				<div class="title text-center">
                    <?= !is_null($visi_misi) ? $visi_misi->description : '' ?>
                </div>
			</div>
			<div class="tab-pane fade" id="nav-pengurus" role="tabpanel" aria-labelledby="nav-pengurus-tab">
				<div class="row">
					@foreach($pengurus as $val)
					<div class="col-md-3">
						<div class="text-center" style="padding:1em;margin: 1em;">
							<div>
								<img style="width: 230px;height: 340px;" src="{{ url('/uploads/'.$val->pic) }}">
							</div>
							<div>
								{{$val->name}}
							</div>
							<div>
								{{$val->position}}
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="tab-pane fade" id="nav-jamaah" role="tabpanel" aria-labelledby="nav-jamaah-tab">
				<table class="table">
					<thead>
						<th>Nama</th>
						<th>Usia</th>
					</thead>
					<tbody>
					@foreach($jamaah as $val)
						<tr>
							<td>
							{{$val->first_name}}
							{{$val->last_name}}
							</td>
							<td>
								{{$val->age}}
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
					
			</div>
			<div class="tab-pane fade" id="nav-keuangan" role="tabpanel" aria-labelledby="nav-keuangan-tab">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-stripped">
							<thead>
								<th>No</th>
								<th>Tanggal</th>
								<th>Keterangan</th>
								<th>Kredit</th>
								<th>Debit</th>
							</thead>
							<tbody>
								<?php $no=1; ?>
								@foreach($table_finance as $f)
								<tr>
									<td>{{$no}}</td>
									<td>{{$f->date}}</td>
									<td>{{$f->information}}</td>
									<td>{{$f->category == 'Pendapatan' ? number_format($f->nominal) : ''}}</td>
									<td>{{$f->category == 'Pengeluaran' ? number_format($f->nominal) : ''}}</td>
								</tr>
								<?php $no += 1; ?>
								@endforeach
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td>Total</td>
									<td>{{number_format($total_donasi->total)}}/td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div style="float: right">
							<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
								Export
							</a>
							<div class="collapse" id="collapseExample">
								<div class="card card-body">
									<a href="{{route('export-pdf-finance',$masjid->id)}}">Export PDF</a>
									<a href="{{route('export-excel-finance',$masjid->id)}}">Export Excel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="barchart_material"></div>
			</div>
			<div class="tab-pane fade" id="nav-pembangunan" role="tabpanel" aria-labelledby="nav-pembangunan-tab">
				<div class="row">
					<div class="col-md-12">
						<div style="float: right">
							<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample">
								Export
							</a>
							<div class="collapse" id="collapseExample1">
								<div class="card card-body">
									<a href="">Export PDF</a>
									<a href="">Export Excel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div id="piechart"></div>
					</div>
					<div class="col-md-6">
						<div id="barchart"></div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="nav-galeri" role="tabpanel" aria-labelledby="nav-galeri-tab">
				<div class="row">
				@foreach($mosque_gallery as $gallery)
					<div class="col-md-4">
						<div class="content-relative">
					  		<img class="thumb" src="{{ url('/uploads/'.$gallery->pic) }}" alt="">
					     	<div class="middle">
					        	<div><?= $gallery->description ?></div>
					     	</div>
						</div>
					</div>
				@endforeach
				</div>
			</div>
			<div class="tab-pane fade" id="nav-acara" role="tabpanel" aria-labelledby="nav-acara-tab">
				<div class="row">
					<div class="col-md-4">
						<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						@foreach($mosque_event as $key => $event)
							<a class="nav-link {{$key == 0 ? 'active' : ''}}" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-{{$event->id}}" role="tab" aria-controls="v-pills-home" aria-selected="true">{{$event->name}}</a>
						@endforeach
						</div>
						
					</div>
					<div class="col-md-8">
						<div class="tab-content" id="v-pills-tabContent">
						@foreach($mosque_event as $key => $event)
							<div class="tab-pane {{$key == 0 ? 'show active' : 'fade' }}" id="v-pills-{{$event->id}}" role="tabpanel" aria-labelledby="v-pills-home-tab">
						  		<center>
						  			<h2><?= $event->name ?></h2>
						  			<img style="width: 100%;height: 180px;" src="{{ url('/uploads/'.$event->pic) }}">
						  			<p><?= $event->description ?></p>
						  		</center>
							</div>
						@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input style="display: none;" type="text" id="lat" value="{{$masjid->latitude}}">
	<input style="display: none;" type="text" id="long" value="{{$masjid->longitude}}">
</section>
<script>
var map;
var markers = [];
var count = 0;

function removeMarker() {
    for (var i = count; i < markers.length; i++) {
      markers[i].setMap(null);
      count++;
      break;
    }
}

function addMarker(location) {
    removeMarker();
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });
    markers.push(marker);
}
function initMap() {                            
    var latitude = <?php echo ($masjid->latitude == '' ? '-6.217028077720872' : $masjid->latitude) ?>;
    var longitude = <?php echo ($masjid->longitude == '' ? '106.84046600990075' : $masjid->longitude) ?>;
    var myLatLng = {lat: latitude, lng: longitude};

    map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 14,
      disableDoubleClickZoom: true, // disable the default map zoom on double click
    });

    addMarker(myLatLng);
}

google.charts.load('current', {'packages':['corechart','bar']});
google.charts.setOnLoadCallback(drawChart);
google.charts.setOnLoadCallback(drawChart2);
google.charts.setOnLoadCallback(drawChart3);

// Draw the chart and set the chart values
function drawChart() {
	var pembangunan = <?php echo json_encode($dev_master); ?>;
	var asd = [['Order', 'percentage']];
	var total = 100;
	pembangunan.forEach(function (item){
		asd.push([item.name, parseFloat(item.weight)]);
	});
  	var data = google.visualization.arrayToDataTable(asd);

  // Optional; add a title and set the width and height of the chart
  var options = {
		title: 'Tahap Pembangunan',
		'legend':'none',
		pieHole: 0.5,
		'width':500,
		'height':500,
		colors: ['#3498db','#2ecc71','#9b59b6','#f39c12'],
      // ,is3D: true
    };

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}

function drawChart2() {
	var pembangunan = <?php echo json_encode($dev); ?>;
	var title = ['Pembangunan'];
	var value = ['2019'];
	var total = 100;
	pembangunan.forEach(function (item){
		title.push(item.dev_name);
		value.push(parseFloat(item.weight));
	});
  	var data = google.visualization.arrayToDataTable([
        title,
        value,
    ]);

  // Optional; add a title and set the width and height of the chart
  var options = {
    width: 400,
    height: 500,
    legend: { position: 'top', maxLines: 3 },
    bar: { groupWidth: '75%' },
    colors: ['#3498db','#2ecc71','#9b59b6','#f39c12'],
    isStacked: true
  };

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.ColumnChart(document.getElementById('barchart'));
  chart.draw(data, options);
}

function drawChart3() {
	var namaMasjid = <?php echo json_encode($masjid->name); ?>;
	var data = <?php echo json_encode($finance); ?>;
	var bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
	var asd = [['Year', 'Pendapatan', 'Pengeluaran']];

	for (var i = 0; i < 12; i++) {
		var namaBulan = bulan[i];
		if (bulan.indexOf(namaBulan) == i) {
			if (data[i] != undefined) {
				asd.push([bulan[i], parseInt(data[i]['pendapatan']), parseInt(data[i]['pengeluaran'])])
			} else {
				asd.push([bulan[i], 0, 0]);
			}
		} else {
			asd.push([bulan[i], 0, 0])
		}
	}

	var data = google.visualization.arrayToDataTable(asd);

	var options = {
	  width:800,
	  height:400,
      title: 'Data Keuangan Masjid',
	  bars: 'vertical',
	};

	var chart = new google.charts.Bar(document.getElementById('barchart_material'));

	chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl5cuWr96CfdeXezgzmToFgf_7Mv8RDmg&callback=initMap" async defer></script>