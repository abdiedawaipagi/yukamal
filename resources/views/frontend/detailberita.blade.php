<section class="section-gap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div style="text-align: center">
					<h2>{{$berita->title}}</h2>
					<div style="margin: 1em;padding: 1em;">
						<img src="{{ url('/uploads/'.$berita->pic) }}">
					</div>
				</div>
				<div>
					<span>Tanggal Acara : {{$berita->date}}</span>
					<div>Keterangan : <?= $berita->description ?></div>
				</div>
			</div>
		</div>
	</div>
</section>