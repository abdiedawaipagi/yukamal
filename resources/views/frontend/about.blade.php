<style type="text/css">
	p {
		color: black;
	}
	.modal-dialog {
		max-width: 700px;
	}
</style>
<section class="banner-area">
	<div class="container">
		<div class="row fullscreen align-items-center justify-content-between">
		<div style="position: absolute;top: 0;right: 0;left: 0;bottom: 0;background-color: #ababab8f;height: 550px;"></div>
			<div class="col-md-12 banner-left" style="text-align: center;">
				<h1>apa itu yukamal?</h1>
			</div>
		</div>
	</div>					
</section>
		<!-- End banner Area -->
<!-- start slider area -->
<section class="top-category-widget-area" style="margin-top: 5%;margin-bottom: 5%;">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<img style="width: 100%" src="{{ url('/') }}/frontend/img/blog/feature-img1.jpg">
			</div>
			<div class="col-md-6">
				<h2>Tentang Kami</h2>
				<p style="font-size: 22px;margin-top: 2%;">Lorem Ipsum is simply dummy text of the
				printing and typesetting industry. Lorem
				Ipsum has been the industry's standard
				dummy text ever since the 1500s when an
				unknown printer 
				</p>
			</div>
		</div>
	</div>
</section>
<!-- end slider area -->
<!-- Start portfolio-area Area -->
<section class="portfolio-area section-gap" id="portfolio" style="background-color: #ababab8f;margin-top: 5%;margin-bottom: 5%;">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-30 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">VISI</h1>
                    <p style="font-size: 25px;margin-top: 2%;">Lorem Ipsum is simply dummy text of the
					printing and typesetting industry. Lorem
					Ipsum has been the industry's standard
					dummy text ever since the 1500s when an
					unknown printer 
					</p>
					<br>
					<h1 class="mb-10">MISI</h1>
	                    <p style="font-size: 25px;margin-top: 2%;">Lorem Ipsum is simply dummy text of the
					printing and typesetting industry. Lorem
					Ipsum has been the industry's standard
					dummy text ever since the 1500s when an
					unknown printer 
					</p>
                </div>
            </div>
        </div>
        
    </div>
</section>
<section class="portfolio-area section-gap" id="portfolio" style="padding: 0;">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-30 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Our Team</h1>
                    <div class="row">
                    	@foreach($team as $tim)
                    	<div class="col-md-6">
							<div class="text-center" style="padding:1em;margin: 1em;">
								<div>
									<img style="width: 100%;" src="{{ url('/uploads/'.$tim->pic) }}">
								</div>
								<div style="padding: 1em 0;">
									<div style="font-size: 1.2em;">
										{{$tim->fullname}}
									</div>
									<div>
										{{$tim->position}}
									</div>
									<div>
										"{{$tim->biography}}"
									</div>
								</div>
								<div style="width: 100%;margin-top: 1em;">
									<div class="row">
										<div class="col-md-4">
											<a style="font-size: 2em;color: blue" href="https://www.facebook.com/{{$tim->facebook}}"><i class="fa fa-facebook"></i></a>
										</div>
										<div class="col-md-4">
											<a style="font-size: 2em;color: red" onClick="javascript:window.open('mailto:{{$tim->email}}', 'Mail');event.preventDefault()" href="mailto:{{$tim->email}}"><i class="fa fa-envelope"></i></a>
										</div>
										<div class="col-md-4">
					                        <a style="font-size: 2em;" href="https://twitter.com/{{$tim->twitter}}"><i class="fa fa-twitter"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
<section class="portfolio-area section-gap" id="portfolio" style="background-color: #ababab8f;margin-top: 5%;">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content">
                <div class="title text-center">
	                <button type="button" class="primary-btn text-uppercase" data-toggle="modal" data-target="#exampleModal">
					  Donasi Yukamal
					</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <div class="row">
					        	<div class="col-md-4">
					        		<img src="{{ url('/uploads/bni.png') }}">
					        		<div style="margin-top: 1em;">
					        			<p>Bank BNI Syariah</p>
					        			<p>999 999 9999</p>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<img src="{{ url('/uploads/mandiri.png') }}">
					        		<div style="margin-top: 1em;">
					        			<p>Bank Mandiri Syariah</p>
					        			<p>999 999 9999</p>
					        		</div>
					        	</div>
					        	<div class="col-md-4">
					        		<img src="{{ url('/uploads/bca.png') }}">
					        		<div style="margin-top: 1em;">
					        			<p>Bank BCA</p>
					        			<p>999 999 9999</p>
					        		</div>
					        	</div>
					        </div>
					        <div class="row">
					        	<div class="container">
					        		<form role="form" method="post" action="{{ route('donasi_yukamal') }}">
										{{ csrf_field() }}
										<input class="form-control" type="hidden" name="user_id" value="{{$user != '' ? $user->id : '0' }}">
										<input class="form-control" type="hidden" name="contributor_name" value="{{$user != '' ? $user->name : 'anon' }}">
										<div class="form-group">
											<input class="form-control" data-type="currency" type="text" name="nominal" placeholder="Nominal" required>
										</div>
										<button type="submit" class="primary-btn text-uppercase text-center btn-block">Yuk Donasi</button>
									</form>
					        	</div>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
            </div>
        </div>
        
    </div>
</section>
<script type="text/javascript">
$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
</script>