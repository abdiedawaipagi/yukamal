<style type="text/css">
	.table th, .table td {
		border: none;
	}
	tr {
		text-align: center;
	}
</style>
<section class="section-gap">
	<div class="container">
		<div class="col-md-12">
			<div class="text-center">
				<h1>Transaksi Saya</h1>
			</div>
			<table class="table table-bordered" style="margin-top: 2em;">
				<thead>
					<th>No Trans</th>
					<th>Masjid</th>
					<th>Nominal</th>
					<th>Bank</th>
					<th>Status</th>
					<th>Action</th>
				</thead>
				<tbody>
				@foreach($transaction as $t)
					<tr>
						<td>{{$t->no_trans}}</td>
						<td>{{$t->mosque_name}}</td>
						<td>Rp {{number_format($t->total)}}</td>
						<td>{{$t->bank}}</td>
						@if($t->status == 0)
						<td>Pending</td>
						@elseif($t->status == 1)
						<td>Dibatalkan</td>
						@elseif($t->status == 3)
						<td>Menunggu konfirmasi</td>
						@elseif($t->status == 2)
						<td>Sukses</td>
						@endif
						<td>
							<a href="{{ route('payment',$t->id) }}"><i class="fa fa-eye"></i></a>
							@if($t->status == 0)
							<a href="{{ route('cancel',$t->id) }}"><i class="fa fa-times"></i></a>
							@endif
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>