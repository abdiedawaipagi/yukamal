<center><b><div style="font-size: 22px;">Data Keuangan Masjid/Mushola {{$masjid}}
Periode Tahun {{date('Y')}}</div></b></center>
<br><br>
<table align="center" width="100%" border="1">
	<tr style="background-color: #6495ed;">
		<td align="center"><b>TANGGAL</b></td>
		<td align="center"><b>KATEGORI</b></td>
		<td align="center"><b>SUB-KATEGORI</b></td>
		<td align="center"><b>INFORMASI</b></td>
		<td align="center"><b>NOMINAL</b></td>
	</tr>
	@foreach($data as $d)
	<tr>
		<td align="center" width="20%">{{$d->date}}</td>
		<td align="left" width="20%">{{$d->category}}</td>
		<td align="center" width="20%">{{$d->sub_category}}</td>
		<td align="left" width="20%">{{$d->information}}</td>
		<td align="right" width="20%">{{number_format(floatval($d->nominal),0,',','.')}}</td>
	</tr>
	@endforeach
</table>