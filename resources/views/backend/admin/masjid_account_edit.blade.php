<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Edit Rekening Bank Masjid / Musholla</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('masjid_account.update', $masjid->mosque_id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                    <input type="hidden" class="form-control" id="form_control_1" value="{{$masjid->mosque_id}}" name="mosque_id">
                    <div class="mt-repeater">
                        <div data-repeater-list="data">
                            @foreach($data as $d)
                            <div data-repeater-item class="row">
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <select class="form-control edited" id="bank" name="bank_id">
                                            <option value=""></option>
                                            @foreach($bank as $b)
                                            <option value="{{$b->id}}" {{($b->id == $d->bank_id ? 'selected' : '')}}>{{$b->code}} - {{$b->name}}</option>
                                            @endforeach
                                        </select>
                                        <label for="bank">Bank</label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="number" class="form-control" id="form_control_1" value="{{$d->no_account}}" name="no_account" maxlength="25">
                                        <label for="form_control_1">No Akun Bank</label>
                                        <span class="help-block">Maksimal 25 Karakter. Masukkan Angka saja.</span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <label class="control-label">&nbsp;</label>
                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <hr>
                        <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                            <i class="fa fa-plus"></i> Tambah No Akun Bank</a>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('masjid_account.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
