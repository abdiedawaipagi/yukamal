<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Edit Pengurus</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" enctype="multipart/form-data" action="{{ route('dkm.update', $data->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                    @if($role == 'admin')
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="mosque_identity">
                            @foreach($mosque as $m)
                            <option value="{{$m->id}}" {{$m->id == $data->mosque_identity ? 'selected' : ''}}>{{$m->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Masjid</label>
                    </div>
                    @else
                    <input type="hidden" class="form-control" id="mosque_identity" value="{{$MosqueDkmUser->mosque_id}}" name="mosque_identity" maxlength="255">
                    @endif
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->name}}" name="name" maxlength="255" required>
                        <label for="form_control_1">Nama</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->position}}" name="position" maxlength="255" required>
                        <label for="form_control_1">Jabatan</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Foto</label><br>
                        <div class="fileinput {{is_null($data->pic) ? 'fileinput-new' : 'fileinput-exist'}}" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                @if(!is_null($data->pic))
                                <img src="{{url('uploads/'.$data->pic)}}">
                                @endif
                            </div>
                            <div>
                                <span class="btn red btn-outline btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="image"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" onClick="remove()" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="image_old" value="{{$data->pic}}" name="image_old" maxlength="255">
                    <input type="hidden" class="form-control" id="image_name" value="{{$data->pic}}" name="image_name" maxlength="255">
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('dkm.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script>
  function remove(){
    document.getElementById("image_old").value = "";
  }
</script>