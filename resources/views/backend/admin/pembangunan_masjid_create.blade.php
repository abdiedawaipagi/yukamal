<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Tambah Pembangunan</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('pembangunan_masjid.store') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    @if($role == 'admin')
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="mosque_id">
                            @foreach($masjid as $m)
                            <option value="{{$m->id}}">{{$m->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Masjid</label>
                    </div>
                    @else
                    <input type="hidden" class="form-control" id="mosque_id" value="{{$MosqueDkmUser->mosque_id}}" name="mosque_id" maxlength="255">
                    @endif
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="dev_id">
                            @foreach($dev as $d)
                            <option value="{{$d->id}}">{{$d->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Pembangunan ID</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" step="any" class="form-control" id="form_control_1" value="" name="weight" maxlength="45" required>
                        <label for="form_control_1">Bobot</label>
                        <span class="help-block">Satuan dalam Persen(%)</span>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('pembangunan_masjid.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->