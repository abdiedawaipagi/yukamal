<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Add Berita</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" enctype="multipart/form-data" action="{{ route('berita.update', $data->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tanggal</label>
                                <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                    <input type="text" class="form-control" value="{{date('d-m-Y', strtotime($data->date))}}" name="date" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->title}}" name="title" maxlength="255" required>
                        <label for="form_control_1">Judul</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Foto</label><br>
                        <div class="fileinput {{is_null($data->pic) ? 'fileinput-new' : 'fileinput-exist'}}" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                @if(!is_null($data->pic))
                                <img src="{{url('uploads/'.$data->pic)}}">
                                @endif
                            </div>
                            <div>
                                <span class="btn red btn-outline btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" value="{{$data->pic}}" name="image"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" onClick="remove()" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="image_old" value="{{$data->pic}}" name="image_old" maxlength="255">
                    <input type="hidden" class="form-control" id="image_name" value="{{$data->pic}}" name="image_name" maxlength="255">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Deskripsi</label>
                        <textarea class="form-control" name="description" id="description" rows="10" cols="50" required>{{$data->description}}</textarea>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('berita.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
  var textarea = document.getElementById("description");
    CKEDITOR.replace(textarea,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;

  function remove(){
    document.getElementById("image_old").value = "";
  }
</script>