<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <h1 class="page-title">Edit Pengguna</h1>
        <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('pengguna.update', $data->user_id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->first_name}}" name="first_name" maxlength="255">
                    <label for="form_control_1">Nama Depan</label>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->last_name}}" name="last_name" maxlength="255">
                    <label for="form_control_1">Nama Belakang</label>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Tanggal Lahir</label>
                            <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy">
                                <input type="text" class="form-control" value="{{date('d-m-Y', strtotime($data->dob))}}" name="date" readonly>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <select class="form-control edited" name="gender">
                        <option value="0" {{$data->gender == '0' ? 'selected' : ''}}>Laki-laki</option>
                        <option value="1" {{$data->gender == '1' ? 'selected' : ''}}>Perempuan</option>
                    </select>
                    <label for="gender">Jenis Kelamin</label>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->email}}" name="email" maxlength="255">
                    <label for="form_control_1">Email</label>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->phone}}" name="phone" maxlength="255">
                    <label for="form_control_1">No Telepon</label>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <label for="form_control_1">Alamat</label>
                    <textarea class="form-control" name="address" rows="3">{{$data->address}}</textarea>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" id="form_control_1" value="{{$data->username}}" name="username" maxlength="255" disabled>
                    <label for="form_control_1">Nama Pengguna</label>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <select class="form-control" name="role" id="role">
                        <option value="admin" {{$data->role == 'admin' ? 'selected' : ''}}>Admin</option>
                        <option value="dkm" {{$data->role == 'dkm' ? 'selected' : ''}}>DKM</option>
                        <option value="member" {{$data->role == 'member' ? 'selected' : ''}}>Member</option>
                    </select>
                    <label for="form_control_1">Peran</label>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label" id="mosque_id" style="{{$data->role == 'dkm' ? 'display: block;' : 'display: none;'}}">
                    <select class="form-control edited" name="mosque_id_dkm">
                        @foreach($mosque as $m)
                        <option value="{{$m->id}}" {{$m->id == $data->mosque_dkm ? 'selected' : ''}}>{{$m->name}}</option>
                        @endforeach
                    </select>
                    <label for="gender">Masjid</label>
                </div>
                <div class="form-group" id="mosque_jamaah" style="{{$data->role == 'member' ? 'display: block;' : 'display: none;'}}">
                    <label for="gender">Jama'ah Masjid</label>
                    @if(!is_null($mosque_ummat))
                    <select class="form-control edited" name="mosque_id_jamaah[]" id="jamaah" multiple>
                        @foreach($mosque as $m)
                            <option value="{{$m->id}}" {{in_array($m->id,$mosque_ummat) ? 'selected' : ''}}>{{$m->name}}</option>
                        @endforeach
                    </select>
                    @else
                    <select class="form-control edited" name="mosque_id_jamaah[]" id="jamaah" multiple>
                        @foreach($mosque as $m)
                            <option value="{{$m->id}}">{{$m->name}}</option>
                        @endforeach
                    </select>
                    @endif
                </div>
            </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('pengguna.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
        <!-- FINISH HERE -->
    </div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/js/backend/jquery.min.js" type="text/javascript"></script>
<link href="{{url('/')}}/css/backend/jquery.multiselect.css" rel="stylesheet" type="text/css" />
<script src="{{url('/')}}/js/backend/jquery.multiselect.js"></script>
<script type="text/javascript">
    $('select[multiple]').multiselect();
    $('#jamaah').multiselect({
        columns: 1,
        placeholder: 'Pilih Masjid'
    });
    $('#role').change(function(){
        if (this.value == 'dkm') {
            $('#mosque_id').show();
            $('#mosque_jamaah').hide();
        }else if (this.value == 'member') {
            $('#mosque_jamaah').show();
            $('#mosque_id').hide();
        }else{
            $('#mosque_jamaah').hide();
            $('#mosque_id').hide();
        }
    });
</script>