<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title"> Kategori </h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
		<div class="portlet-body">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a href="{{ route('category.create') }}">
                                <button class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>
                        <td align="center" class="bold"> Nama </td>
                        <td align="center" class="bold"> Actions </td>
                    </tr>
                </thead>
                <tbody>
                	@foreach($data as $val)
                    <tr class="odd gradeX">
                        <td align="left"> {{$val->nama}} </td>
                        <td align="center">
                            <a href="{{route('category.edit', $val->id)}}"><i class="fa fa-pencil"></i></a> | 
                            <a data-target="#static-{{$val->id}}" id="modal_delete" data-id="{{$val->id}}" data-toggle="modal"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    <div id="static-{{$val->id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                        <form method="post" action="{{route('category.destroy', $val->id)}}">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                        <div class="modal-body">
                            <p> Apakah Anda yakin ini menghapus? </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                            <button type="submit"  class="btn red">Delete</button>
                        </div>
                        </form>
                    </div>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END CONTENT -->