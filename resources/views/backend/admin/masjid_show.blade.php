<style>          
  #map { 
    height: 300px;    
    width: 600px;            
  }          
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Detail Masjid / Musholla</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <div class="form-body">
                <a href="{{route('masjid.edit', $data->id)}}"><button type="button" class="btn blue">Update</button></a>
                @if($role == 'admin')
                <a data-target="#static-{{$data->id}}" id="modal_delete" data-id="{{$data->id}}" data-toggle="modal"><button type="button" class="btn red">Delete</button></a>
                <div id="static-{{$data->id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form method="post" action="{{route('masjid.destroy', $data->id)}}">
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="modal-body">
                        <p> Apakah Anda yakin ini menghapus? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                        <button type="submit"  class="btn red">Delete</button>
                    </div>
                    </form>
                </div>
                @endif
                <br><br>
                <table class="table">
                    <tbody>
                        <tr>
                            <th width="150">Kategori</th>
                            <td width="30">:</td>
                            @if($data->type == 0)
                            <td>Masjid</td>
                            @else
                            <td>Mushola</td>
                            @endif
                        </tr>
                        <tr>
                            <th>Kode Masjid</th>
                            <td>:</td>
                            <td>{{$data->code}}</td>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <td>:</td>
                            <td><?= $data->name;?></td>
                        </tr>
                        <tr>
                            <th>ID Masjid (Kemenag)</th>
                            <td>:</td>
                            <td>{{$data->identity}}</td>
                        </tr>
                        <tr>
                            <th>Luas Tanah</th>
                            <td>:</td>
                            <td>{{$data->surface_area}}</td>
                        </tr>
                        <tr>
                            <th>Luas Bangunan</th>
                            <td>:</td>
                            <td>{{$data->building_area}}</td>
                        </tr>
                        <tr>
                            <th>Status Tanah</th>
                            <td>:</td>
                            <td>{{$data->los}}</td>
                        </tr>
                        <tr>
                            <th>Tahun Berdiri</th>
                            <td>:</td>
                            <td>{{$data->since}}</td>
                        </tr>
                        <tr>
                            <th>Bank</th>
                            <td>:</td>
                            <td>{{is_null($bank) ? '' : $bank->name}}</td>
                        </tr>
                        <tr>
                            <th>Nomor Rekening</th>
                            <td>:</td>
                            <td>{{$data->rek}}</td>
                        </tr>
                        <tr>
                            <th>Provinsi</th>
                            <td>:</td>
                            <td>{{!is_null($provinces) ? $provinces->name : ''}}</td>
                        </tr>
                        <tr>
                            <th>Kota/Kabupaten</th>
                            <td>:</td>
                            <td>{{!is_null($regencies) ? $regencies->name : ''}}</td>
                        </tr>
                        <tr>
                            <th>Kecamatan</th>
                            <td>:</td>
                            <td>{{!is_null($districts) ? $districts->name : ''}}</td>
                        </tr>
                        <tr>
                            <th>Kelurahan</th>
                            <td>:</td>
                            <td>{{!is_null($villages) ? $villages->name : ''}}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>:</td>
                            <td>{{$data->address}}</td>
                        </tr>
                        <tr>
                            <th>Latitude</th>
                            <td>:</td>
                            <td>{{$data->latitude}}</td>
                        </tr>
                        <tr>
                            <th>Longitude</th>
                            <td>:</td>
                            <td>{{$data->longitude}}</td>
                        </tr>
                        <tr>
                            <th>Foto</th>
                            <td>:</td>
                            <td><img style="width: 360px; height: 270px;" src="{{url('uploads/'.$data->pic)}}"></td>
                        </tr>
                        <tr>
                            <th>Deskripsi</th>
                            <td>:</td>
                            <td><?= $data->description?></td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <div id="map"></div>
                </div>
                <a href="{{route('masjid')}}"><button type="button" class="btn default">Back</button></a>
            </div>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    var map;
    var markers = [];
    var count = 0;

    function removeMarker() {
        for (var i = count; i < markers.length; i++) {
          markers[i].setMap(null);
          count++;
          break;
        }
    }

    function addMarker(location) {
        removeMarker();
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        markers.push(marker);
    }
    
    function initMap() {
        var latitude = <?php echo ($data->latitude == '' ? '-6.217028077720872' : $data->latitude) ?>;
        var longitude = <?php echo ($data->longitude == '' ? '106.84046600990075' : $data->longitude) ?>;
        var myLatLng = {lat: latitude, lng: longitude};

        map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 14,
          disableDoubleClickZoom: true, // disable the default map zoom on double click
        });

        addMarker(myLatLng);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl5cuWr96CfdeXezgzmToFgf_7Mv8RDmg&callback=initMap" async defer></script>
<script src="{{url('/')}}/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
  var textarea = document.getElementById("description");
    CKEDITOR.replace(textarea,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;
</script>