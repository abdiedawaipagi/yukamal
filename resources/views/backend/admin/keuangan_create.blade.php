<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Add Keuangan</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('keuangan.store') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tanggal</label>
                                <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control" value="{{date('d-m-Y')}}" name="date" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($role == 'admin')
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="mosque_id">
                            @foreach($mosque as $m)
                            <option value="{{$m->id}}">{{$m->name}}</option>
                            @endforeach
                        </select>
                        <label for="status">Masjid</label>
                    </div>
                    @else
                    <input type="hidden" class="form-control" value="{{$MosqueDkmUser->mosque_id}}" name="mosque_id" maxlength="255">
                    @endif
                    @if($role == 'admin')
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="{{$user_id}}" name="user_id" maxlength="255">
                        <label for="form_control_1">User</label>
                        <span class="help-block">Maksimal 25 Karakter</span>
                    </div>
                    @else
                        <input type="hidden" class="form-control" id="form_control_1" value="{{Auth::user()->id}}" name="user_id" maxlength="255">
                    @endif
                    <div class="mt-repeater">
                        <div data-repeater-list="finance">
                            <div data-repeater-item class="row" style="margin-bottom: 1em;">
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <select class="form-control edited" name="category_id" onChange="categoryId(this)">
                                            @foreach($category as $c)
                                            <option value="{{$c->id}}">{{$c->nama}}</option>
                                            @endforeach
                                        </select>
                                        <label for="gender">Kategori</label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <select class="form-control edited" name="sub_category_id" id="sub_category_id[]">
                                            @foreach($sub_category as $sc)
                                                <option value="{{$sc->id}}">{{$sc->nama}}</option>
                                            @endforeach
                                        </select>
                                        <label for="gender">Sub-Kategori</label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" name="information" class="form-control" id="form_control_1" required>
                                        <label for="form_control_1">Keterangan</label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" name="nominal" class="form-control" id="form_control_1" value="Rp" data-type="currency" placeholder="Rp1.000.000,00" required>
                                        <label for="form_control_1">Nominal</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">&nbsp;</label>
                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                            <i class="fa fa-plus"></i> Add Variation</a>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('keuangan.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/js/backend/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function categoryId(e) {
        var category_id = e.value;
        var my_url = "<?= url('/get_subcategory')?>"+'/'+category_id;

        if (category_id == '') {
            $('select[name='+e.name+']').html('');
        } else {
            $.ajax({
                url: my_url,
                dataType: 'json',
                success: function (data) {
                    $('select[name="'+e.name.substr(0,10)+'[sub_category_id]"]').prop('disabled', false);
                    $('select[name="'+e.name.substr(0,10)+'[sub_category_id]"]').html('');
                    data.forEach(function(item, index){
                        $('select[name="'+e.name.substr(0,10)+'[sub_category_id]"]').append('<option value="'+item.id+'">'+item.nama+'</option>');
                    });
                }
            });
        }
    }

        // Jquery Dependency

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(",") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(",");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "Rp" + left_side + "," + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "Rp" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ",00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
</script>