<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title"> Data Jama'ah</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
		<div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>
                        <td align="center" class="bold"> Nama </td>
                        <td align="center" class="bold"> Usia </td>
                        <td align="center" class="bold"> Jenis Kelamin </td>
                        <td align="center" class="bold"> Masjid </td>
                    </tr>
                </thead>
                <tbody>
                	@foreach($data as $val)
                    <tr class="odd gradeX">
                        <td align="center"> {{$val->first_name.' '.$val->last_name}} </td>
                        <?php
                            $dob = new DateTime($val->dob);
                            $today = new DateTime();
                            $diff = $today->diff($dob);
                            $usia = $diff->y;
                        ?>
                        <td align="center"> {{$usia}} Tahun</td>
                        <td align="center"> {{$val->gender == '0' ? 'Laki-Laki' : 'Perempuan'}} </td>
                        <td align="center"> {{$val->name}} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->