<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <h1 class="page-title">Edit Kota</h1>
        <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('kota.update', $data->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="province" name="province_id">
                            @foreach($province as $p)
                            <option value="{{$p->id}}" {{$p->id == $data->province_id ? 'selected' : ''}}>{{$p->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Provinsi</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->name}}" name="name" maxlength="255" required>
                        <label for="form_control_1">Nama</label>
                        <span class="help-block">Maksimal 25 Karakter</span>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('kota.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
        <!-- FINISH HERE -->
    </div>
</div>
<!-- END CONTENT -->