<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Donasi YukAmal</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" enctype="multipart/form-data" action="{{ route('donasiyukamal.update', $data->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->no_trans}}" name="no_trans" disabled>
                        <label for="form_control_1">Nomor Transaksi</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{date('d/m/Y', strtotime($data->date))}}" name="date" disabled>
                        <label for="form_control_1">Tanggal</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->contributor_name}}" name="contributor_name" disabled>
                        <label for="form_control_1">Nama Donatur</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{number_format($data->nominal,2,',','.')}}" name="nominal" disabled>
                        <label for="form_control_1">Nominal</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Foto</label><br>
                        <div class="fileinput {{is_null($data->pic) ? 'fileinput-new' : 'fileinput-exist'}}" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                @if(!is_null($data->pic))
                                <img src="{{url('uploads/'.$data->pic)}}">
                                @endif
                            </div>
                            <div>
                                <span class="btn red btn-outline btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="image"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" onClick="remove()" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="image_old" value="{{$data->pic}}" name="image_old" maxlength="255">
                    <input type="hidden" class="form-control" id="image_name" value="{{$data->pic}}" name="image_name" maxlength="255">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="status">
                            <option value="0" {{$data->status == '0' ? 'selected' : ''}}>Proses</option>
                            <option value="1" {{$data->status == '1' ? 'selected' : ''}}>Gagal</option>
                            <option value="2" {{$data->status == '2' ? 'selected' : ''}}>Sukses</option>
                        </select>
                        <label for="status">Status</label>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('donasiyukamal.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script>
  function remove(){
    document.getElementById("image_old").value = "";
  }
</script>