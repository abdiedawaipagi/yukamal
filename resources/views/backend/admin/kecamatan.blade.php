<script src="{{url('/')}}/css/backend/jquery.dataTables.min.css" type="text/javascript"></script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title"> Kecamatan</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
		<div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datalist">
                <thead>
                    <tr>
                        <td align="center" class="bold" width="40%"> Nama </td>
                        <td align="center" class="bold"> Kota </td>
                        <td align="center" class="bold"> Actions </td>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td align="center" class="bold" width="40%"> Nama </td>
                        <td align="center" class="bold"> Kota </td>
                        <td align="center" class="bold"> Actions </td>
                    </tr>
                </tfoot>
            </table>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/js/jquery.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ajaxStart(function() {
        $(".loading").css("display","block");
    }).ajaxStop(function() {
        $(".loading").css("display","none");
    });
    $(function() {
        $('#datalist').DataTable({
            "processing": true,
            "serverSide": true,
            "autoWidth": true,
            "language": {
                "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>'},
            "ajax" : {
                "url" : "{{route('get-all-districts')}}",
                "type":"GET",
            },
            "columns": [
                { "data" : "name"},
                { "data" : "regency_name"},
                { "data" : "actions","searchable":false,"orderable":false},
            ],initComplete: function () {
                $(".loading").css("display","none");
            },
        });
    });
</script>