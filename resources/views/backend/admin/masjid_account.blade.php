<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title"> Rekening Bank Masjid</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
		<div class="portlet-body">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a href="{{ route('masjid_account.create') }}">
                                <button class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="{{ route('masjid_account.edit', $masjid->mosque_id) }}">
                                <button class="btn sbold yellow"> Edit
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>
                        <td align="center" class="bold"> Nama </td>
                        <td align="center" class="bold"> No Akun </td>
                    </tr>
                </thead>
                <tbody>
                	@foreach($data as $val)
                    <tr class="odd gradeX">
                        <td align="left"> {{$val->bank_name}} </td>
                        <td align="center"> {{$val->no_account}} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->