<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Add Event Masjid / Musholla</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <div class="form-body">
                <a href="{{route('event.edit', $data->id)}}"><button type="button" class="btn blue">Update</button></a>
                <a data-target="#static-{{$data->id}}" id="modal_delete" data-id="{{$data->id}}" data-toggle="modal"><button type="button" class="btn red">Delete</button></a>
                <div id="static-{{$data->id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form method="post" action="{{route('event.destroy', $data->id)}}">
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="modal-body">
                        <p> Apakah Anda yakin ini menghapus? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                        <button type="submit"  class="btn red">Delete</button>
                    </div>
                    </form>
                </div>
                <br><br>
                <table class="table">
                    <tbody>
                        <tr>
                            <th width="150">Masjid</th>
                            <td width="30">:</td>
                            <td>
                                @foreach($masjid as $m)
                                @if($m->id == $data->mosque_id)
                                    {{$m->name}}
                                @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <td>:</td>
                            <td>{{date('d-m-Y',strtotime($data->date))}}</td>
                        </tr>
                        <tr>
                            <th>Judul</th>
                            <td>:</td>
                            <td><?= $data->name;?></td>
                        </tr>
                        <tr>
                            <th>Deskripsi</th>
                            <td>:</td>
                            <td><?= $data->description;?></td>
                        </tr>
                        <tr>
                            <th>Foto</th>
                            <td>:</td>
                            <td>
                                @if(!is_null($data->pic))
                                    <img style="width: 360px; height: 270px;" src="{{url('uploads/'.$data->pic)}}">
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-actions noborder">
                <a href="{{route('event.index')}}"><button type="button" class="btn default">Cancel</button></a>
            </div>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
  var textarea = document.getElementById("description");
    CKEDITOR.replace(textarea,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;
</script>