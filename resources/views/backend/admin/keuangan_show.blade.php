<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Detail Keuangan</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <div class="form-body">
                <a href="{{route('keuangan.edit', $detail->id)}}"><button type="button" class="btn blue">Update</button></a>
                <a data-target="#static-{{$detail->id}}" id="modal_delete" data-id="{{$detail->id}}" data-toggle="modal"><button type="button" class="btn red">Delete</button></a>
                <div id="static-{{$detail->id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form method="post" action="{{route('keuangan.destroy', $detail->id)}}">
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="modal-body">
                        <p> Apakah Anda yakin ini menghapus? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                        <button type="submit"  class="btn red">Delete</button>
                    </div>
                    </form>
                </div>
                <br><br>
                <div class="row">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th width="150">Tanggal</th>
                                <td width="30">:</td>
                                <td>{{date('d-m-Y', strtotime($data->date))}}</td>
                            </tr>
                            <tr>
                                <th>Masjid</th>
                                <td>:</td>
                                <td>{{$mosque->name}}</td>
                            </tr>
                            <tr>
                                <th>User</th>
                                <td>:</td>
                                <td>{{$user->name}}</td>
                            </tr>
                            <tr>
                                <th>Kategori</th>
                                <td>:</td>
                                <td>{{$category->nama}}</td>
                            </tr>
                            <tr>
                                <th>Sub-Kategori</th>
                                <td>:</td>
                                <td>{{$sub_category->nama}}</td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td>:</td>
                                <td>{{$detail->information}}</td>
                            </tr>
                            <tr>
                                <th>Nominal</th>
                                <td>:</td>
                                <td>Rp.{{number_format($detail->nominal,2,',','.')}}</td>
                            </tr>
                        </tbody>
                    </table>
            <div class="form-actions noborder">
                <a href="{{route('keuangan.index')}}"><button type="button" class="btn default">Cancel</button></a>
            </div>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script src="{{url('/')}}/js/backend/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function categoryId(e) {
        var category_id = e.value;
        var my_url = "<?= url('/get_subcategory')?>"+'/'+category_id;

        if (category_id == '') {
            $('select[name='+e.name+']').html('');
        } else {
            $.ajax({
                url: my_url,
                dataType: 'json',
                success: function (data) {
                    $('select[name="'+e.name.substr(0,10)+'[sub_category_id]"]').prop('disabled', false);
                    $('select[name="'+e.name.substr(0,10)+'[sub_category_id]"]').html('');
                    data.forEach(function(item, index){
                        $('select[name="'+e.name.substr(0,10)+'[sub_category_id]"]').append('<option value="'+item.id+'">'+item.nama+'</option>');
                    });
                }
            });
        }
    }
</script>