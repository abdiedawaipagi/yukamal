<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Tambah Kategori</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('subcategory.store') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="category_id">
                            @foreach($category as $cat)
                            <option value="{{$cat->id}}">{{$cat->nama}}</option>
                            @endforeach
                        </select>
                        <label for="mosque">Kategori</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="nama" maxlength="45">
                        <label for="form_control_1">Nama Kategori</label>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('subcategory.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->