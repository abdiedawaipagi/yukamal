<style>          
  #map { 
    height: 300px;    
    width: 600px;            
  }          
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Edit Informasi Masjid / Musholla</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
        <div class="portlet-body form">
            <form role="form" method="post" enctype="multipart/form-data" action="{{ route('update.masjid', $data->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="type">
                            <option value="0" {{$data->type == 0 ? 'selected' : ''}}>Masjid</option>
                            <option value="1" {{$data->type == 1 ? 'selected' : ''}}>Musholla</option>
                        </select>
                        <label for="province">Tipe</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->code}}" name="code" maxlength="10" disabled>
                        <label for="form_control_1">Kode Masjid</label>
                        <span class="help-block">Maksimal 10 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->name}}" name="name" maxlength="255" required>
                        <label for="form_control_1">Nama Masjid</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" id="form_control_1" value="{{$data->identity}}" name="identity" maxlength="25">
                        <label for="form_control_1">ID Masjid (Kemenag)</label>
                        <span class="help-block">Maksimal 25 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="{{$data->surface_area}}" name="surface_area" required>
                        <label for="form_control_1">Luas Tanah</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="{{$data->building_area}}" name="building_area" required>
                        <label for="form_control_1">Luas Bangunan</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="los">
                            <option value="Wakaf" {{$data->los == 'Wakaf' ? 'selected' : ''}}>Wakaf</option>
                            <option value="SHM" {{$data->los == 'SHM' ? 'selected' : ''}}>SHM</option>
                        </select>
                        <label for="los">Tipe</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="{{$data->since}}" name="since" required>
                        <label for="form_control_1">Tahun Berdiri</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="bank" name="bank_id">
                            @foreach($banks as $b)
                            <option value="{{$b->id}}" {{$data->bank_id == $b->id ? 'selected' : ''}}>{{$b->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Bank</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="{{$data->rek}}" name="rek" required>
                        <label for="form_control_1">Nomor Rekening</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="{{$data->estimate}}" name="estimate" required>
                        <label for="form_control_1">Estimasi Donasi</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="date" class="form-control" id="form_control_1" value="{{$data->estimate_date}}" name="estimate_date" required>
                        <label for="form_control_1">Estimasi Tanggal</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="provinsi" name="province_id">
                            @foreach($province as $p)
                            <option value="{{$p->id}}" {{$data->province_id == $p->id ? 'selected' : ''}}>{{$p->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Provinsi</label>
                    </div><div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="kota" name="city_id">
                            @foreach($regencies as $r)
                            <option value="{{$r->id}}" {{$data->city_id == $r->id ? 'selected' : ''}}>{{$r->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Kota/Kabupaten</label>
                    </div><div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="kecamatan" name="kec_id">
                            @foreach($districts as $d)
                            <option value="{{$d->id}}" {{$data->kec_id == $d->id ? 'selected' : ''}}>{{$d->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Kecamatan</label>
                    </div><div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="kelurahan" name="kel_id">
                            @foreach($villages as $v)
                            <option value="{{$v->id}}" {{$data->kel_id == $v->id ? 'selected' : ''}}>{{$v->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Kelurahan</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <textarea class="form-control" rows="3" name="address" maxlength="500" required>{{$data->address}}</textarea>
                        <label for="form_control_1">Alamat</label>
                        <span class="help-block">Maksimal 500 Karakter</span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <input type="text" class="form-control" id="latclicked" value="{{$data->latitude}}" name="latitude" required>
                                <label for="form_control_1">Latitude</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <input type="text" class="form-control" id="longclicked" value="{{$data->longitude}}" name="longitude" required>
                                <label for="form_control_1">Longitude</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="map"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Foto</label><br>
                        <div class="fileinput {{is_null($data->pic) ? 'fileinput-new' : 'fileinput-exist'}}" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                @if(!is_null($data->pic))
                                <img src="{{url('uploads/'.$data->pic)}}">
                                @endif
                            </div>
                            <div>
                                <span class="btn red btn-outline btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" value="{{$data->pic}}" name="image"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" onClick="remove()" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="image_old" value="{{$data->pic}}" name="image_old" maxlength="255">
                    <input type="hidden" class="form-control" id="image_name" value="{{$data->pic}}" name="image_name" maxlength="255">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Deskripsi</label>
                        <textarea class="form-control" name="description" id="description" rows="10" cols="50" required>{{$data->description}}</textarea>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('masjid')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    var map;
    var markers = [];
    var count = 0;

    function removeMarker() {
        for (var i = count; i < markers.length; i++) {
          markers[i].setMap(null);
          count++;
          break;
        }
    }

    function addMarker(location) {
        removeMarker();
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        markers.push(marker);
    }
    
    function initMap() {                            
        var latitude = <?php echo ($data->latitude == '' ? '-6.217028077720872' : $data->latitude) ?>;
        var longitude = <?php echo ($data->longitude == '' ? '106.84046600990075' : $data->longitude) ?>;
        var myLatLng = {lat: latitude, lng: longitude};

        map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 14,
          disableDoubleClickZoom: true, // disable the default map zoom on double click
        });

        map.addListener('click', function(event) {
            addMarker(event.latLng);
            document.getElementById('latclicked').value = event.latLng.lat();
            document.getElementById('longclicked').value =  event.latLng.lng();
        });

        addMarker(myLatLng);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl5cuWr96CfdeXezgzmToFgf_7Mv8RDmg&callback=initMap" async defer></script>
<script src="{{url('/')}}/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="{{url('/')}}/js/backend/jquery.min.js" type="text/javascript"></script>
<script>
  var textarea = document.getElementById("description");
    CKEDITOR.replace(textarea,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;

    $("#provinsi").change(function(){
        var province_id = $("#provinsi").val();
        var my_url = "<?= url('/city')?>"+'/'+province_id;
        
        if (province_id == '') {
            $('#kota').html('');
            $('#kecamatan').html('');
            $('#kelurahan').html('');
        } else {
            $.ajax({
                url: my_url,
                dataType: 'json',
                success: function (data) {
                    $('#kota').prop('disabled', false);
                    $('#kota').html('');
                    $('#kecamatan').html('');
                    $('#kelurahan').html('');
                    $('#kota').append('<option value=""></option>');
                    data.forEach(function(item, index){
                        $('#kota').append('<option value="'+item.id+'">'+item.name+'</option>');
                    });
                }
            });
        }
    });

    $("#kota").change(function(){
        var city_id = $("#kota").val();
        var my_url = "<?= url('/districts')?>"+'/'+city_id;

        $.ajax({
            url: my_url,
            dataType: 'json',
            success: function (data) {
                $('#kecamatan').prop('disabled', false);
                $('#kecamatan').html('');
                $('#kelurahan').html('');
                $('#kecamatan').append('<option value=""></option>');
                data.forEach(function(item, index){
                    $('#kecamatan').append('<option value="'+item.id+'">'+item.name+'</option>');
                });
            }
        });
    });

    $("#kecamatan").change(function(){
        var kecamatan_id = $("#kecamatan").val();
        var my_url = "<?= url('/villages')?>"+'/'+kecamatan_id;

        $.ajax({
            url: my_url,
            dataType: 'json',
            success: function (data) {
                $('#kelurahan').prop('disabled', false);
                $('#kelurahan').html('');
                data.forEach(function(item, index){
                    $('#kelurahan').append('<option value="'+item.id+'">'+item.name+'</option>');
                });
            }
        });
    });
</script>