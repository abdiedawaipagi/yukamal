<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <h1 class="page-title">Add Kecamatan</h1>
        <!-- INPUT CONTENT BELOW HERE -->
        <div class="portlet-body form">
            <form role="form" method="post" action="{{ route('kecamatan.store') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="regency" name="regency_id">
                            @foreach($regency as $r)
                            <option value="{{$r->id}}">{{$r->name}}</option>
                            @endforeach
                        </select>
                        <label for="regency">Kota</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="name" maxlength="255" required>
                        <label for="form_control_1">Nama</label>
                        <span class="help-block">Maksimal 25 Karakter</span>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('kecamatan.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
        <!-- FINISH HERE -->
    </div>
</div>
<!-- END CONTENT -->