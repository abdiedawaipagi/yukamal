<!--BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title"> Daftar Pengguna</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
		<div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>
                        <td align="center" class="bold" width="30%"> Nama </td>
                        <td align="center" class="bold"> User Pengguna </td>
                        <td align="center" class="bold"> Email </td>
                        <td align="center" class="bold"> Peran </td>
                        <td align="center" class="bold"> Actions </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $val)
                    <tr class="odd gradeX">
                        <td align="center"> {{$val->first_name}} {{$val->last_name}} </td>
                        <td align="center"> {{$val->username}} </td>
                        <td align="center"> {{$val->email}} </td>
                        <td align="center"> {{$val->role}} </td>
                        <td align="center">
                            <a href="{{route('pengguna.show', $val->user_id)}}"><i class="fa fa-eye"></i></a> |
                            <a href="{{route('pengguna.edit', $val->user_id)}}"><i class="fa fa-pencil"></i></a> |
                            <a data-target="#static-{{$val->user_id}}" id="modal_delete" data-id="{{$val->user_id}}" data-toggle="modal"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    <div id="static-{{$val->user_id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                        <form method="post" action="{{route('pengguna.destroy', $val->user_id)}}">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                        <div class="modal-body">
                            <p> Apakah Anda yakin ini menghapus? </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                            <button type="submit"  class="btn red">Delete</button>
                        </div>
                        </form>
                    </div>
                    @endforeach
                </tbody>
            </table>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT