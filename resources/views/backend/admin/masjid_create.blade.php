<style>          
  #map { 
    height: 300px;    
    width: 600px;            
  }          
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Add Masjid / Musholla</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
        <div class="portlet-body form">
            <form role="form" method="post" enctype="multipart/form-data" action="{{ route('save-masjid') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="type">
                            <option value="0">Masjid</option>
                            <option value="1">Musholla</option>
                        </select>
                        <label for="province">Tipe</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="code" maxlength="10" disabled>
                        <label for="form_control_1">Kode Masjid (Automatic)</label>
                        <span class="help-block">Maksimal 10 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="name" maxlength="255" required>
                        <label for="form_control_1">Nama Masjid</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" id="form_control_1" value="" name="identity" maxlength="25">
                        <label for="form_control_1">ID Masjid (Kemenag)</label>
                        <span class="help-block">Maksimal 25 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="" name="surface_area" required>
                        <label for="form_control_1">Luas Tanah</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="" name="building_area" required>
                        <label for="form_control_1">Luas Bangunan</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" name="los">
                            <option value="Wakaf">Wakaf</option>
                            <option value="SHM">SHM</option>
                        </select>
                        <label for="los">Tipe</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="" name="since" required>
                        <label for="form_control_1">Tahun Berdiri</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="bank" name="bank_id">
                            @foreach($banks as $b)
                            <option value="{{$b->id}}">{{$b->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Bank</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="" name="rek" required>
                        <label for="form_control_1">Nomor Rekening</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="number" class="form-control" id="form_control_1" value="" name="estimate" required>
                        <label for="form_control_1">Estimasi Donasi</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="date" class="form-control" id="form_control_1" value="" name="estimate_date" required>
                        <label for="form_control_1">Estimasi Tanggal</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="provinsi" name="province_id">
                            <option value=""></option>
                            @foreach($province as $p)
                            <option value="{{$p->id}}">{{$p->name}}</option>
                            @endforeach
                        </select>
                        <label for="province">Provinsi</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="kota" name="city_id">
                        </select>
                        <label for="province">Kota/Kabupaten</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="kecamatan" name="kec_id">
                        </select>
                        <label for="province">Kecamatan</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control edited" id="kelurahan" name="kel_id">
                        </select>
                        <label for="province">Kelurahan</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <textarea class="form-control" rows="3" name="address" maxlength="500" required></textarea>
                        <label for="form_control_1">Alamat</label>
                        <span class="help-block">Maksimal 500 Karakter</span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <input type="text" class="form-control" id="latclicked" value="" name="latitude" required>
                                <label for="form_control_1">Latitude</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <input type="text" class="form-control" id="longclicked" value="" name="longitude" required>
                                <label for="form_control_1">Longitude</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="map"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Foto</label><br>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                            <div>
                                <span class="btn red btn-outline btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="image"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <label for="form_control_1">Deskripsi</label>
                        <textarea class="form-control" name="description" id="description" rows="10" cols="50" required></textarea>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('masjid')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    var map;
    var markers = [];
    var count = 0;

    function removeMarker() {
        for (var i = count; i < markers.length; i++) {
          markers[i].setMap(null);
          count++;
          break;
        }
    }

    function addMarker(location) {
        removeMarker();
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        markers.push(marker);
    }
    
    function initMap() {                            
        var latitude = -6.217028077720872; // YOUR LATITUDE VALUE
        var longitude = 106.84046600990075; // YOUR LONGITUDE VALUE
        var myLatLng = {lat: latitude, lng: longitude};

        map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 14,
          disableDoubleClickZoom: true, // disable the default map zoom on double click
        });

        map.addListener('click', function(event) {
            addMarker(event.latLng);
            document.getElementById('latclicked').value = event.latLng.lat();
            document.getElementById('longclicked').value =  event.latLng.lng();
        });

        addMarker(myLatLng);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl5cuWr96CfdeXezgzmToFgf_7Mv8RDmg&callback=initMap" async defer></script>
<script src="{{url('/')}}/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="{{url('/')}}/js/backend/jquery.min.js" type="text/javascript"></script>
<script>
  var textarea = document.getElementById("description");
    CKEDITOR.replace(textarea,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;

    $("#provinsi").change(function(){
        var province_id = $("#provinsi").val();
        var my_url = "<?= url('/city')?>"+'/'+province_id;
        
        if (province_id == '') {
            $('#kota').html('');
            $('#kecamatan').html('');
            $('#kelurahan').html('');
        } else {
            $.ajax({
                url: my_url,
                dataType: 'json',
                success: function (data) {
                    $('#kota').prop('disabled', false);
                    $('#kota').html('');
                    $('#kota').append('<option value=""></option>');
                    data.forEach(function(item, index){
                        $('#kota').append('<option value="'+item.id+'">'+item.name+'</option>');
                    });
                }
            });
        }
    });

    $("#kota").change(function(){
        var city_id = $("#kota").val();
        var my_url = "<?= url('/districts')?>"+'/'+city_id;

        $.ajax({
            url: my_url,
            dataType: 'json',
            success: function (data) {
                $('#kecamatan').prop('disabled', false);
                $('#kecamatan').html('');
                $('#kecamatan').append('<option value=""></option>');
                data.forEach(function(item, index){
                    $('#kecamatan').append('<option value="'+item.id+'">'+item.name+'</option>');
                });
            }
        });
    });

    $("#kecamatan").change(function(){
        var kecamatan_id = $("#kecamatan").val();
        var my_url = "<?= url('/villages')?>"+'/'+kecamatan_id;

        $.ajax({
            url: my_url,
            dataType: 'json',
            success: function (data) {
                $('#kelurahan').prop('disabled', false);
                $('#kelurahan').html('');
                data.forEach(function(item, index){
                    $('#kelurahan').append('<option value="'+item.id+'">'+item.name+'</option>');
                });
            }
        });
    });
</script>