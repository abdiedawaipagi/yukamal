<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
	    <h1 class="page-title">Add User</h1>
	    <!-- INPUT CONTENT BELOW HERE -->
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('alert'))
            <div class="alert alert-danger">
                {{ session()->get('alert') }}
            </div>
        @endif
        <div class="portlet-body form">
            <form role="form" method="post" enctype="multipart/form-data" action="{{ route('useryukamal.store') }}">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="name" maxlength="255" required>
                        <label for="form_control_1">Nama</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" class="form-control" id="form_control_1" value="" name="username" maxlength="255" required>
                        <label for="form_control_1">Username</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="email" class="form-control" id="form_control_1" value="" name="email" maxlength="255" required>
                        <label for="form_control_1">Email</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <select class="form-control" name="role">
                            <option value="admin">Admin</option>
                            <option value="dkm">DKM</option>
                        </select>
                        <label for="form_control_1">Role</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="password" class="form-control" id="form_control_1" value="" name="password" maxlength="255" required>
                        <label for="form_control_1">Password</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="password" class="form-control" id="form_control_1" value="" name="password_confirm" maxlength="255" required>
                        <label for="form_control_1">Password Confirmation</label>
                        <span class="help-block">Maksimal 255 Karakter</span>
                    </div>
                </div>
                <div class="form-actions noborder">
                    <button type="submit" class="btn blue">Submit</button>
                    <a href="{{route('useryukamal.index')}}"><button type="button" class="btn default">Cancel</button></a>
                </div>
            </form>
        </div>
		<!-- FINISH HERE -->
	</div>
</div>
<!-- END CONTENT -->